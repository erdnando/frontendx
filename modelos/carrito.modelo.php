<?php

require_once "conexion.php";

class ModeloCarrito{

	/*=============================================
	MOSTRAR TARIFAS
	=============================================*/

	static public function mdlMostrarTarifas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$tmt =null;

	}

	/*=============================================
	NUEVAS COMPRAS
	=============================================*/

	static public function mdlNuevasCompras($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_usuario, id_producto, metodo, email, direccion, pais, cantidad, detalle, pago) VALUES (:id_usuario, :id_producto, :metodo, :email, :direccion, :pais, :cantidad, :detalle, :pago)");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);
		$stmt->bindParam(":metodo", $datos["metodo"], PDO::PARAM_STR);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
		$stmt->bindParam(":pais", $datos["pais"], PDO::PARAM_STR);
		$stmt->bindParam(":cantidad", $datos["cantidad"], PDO::PARAM_STR);
		$stmt->bindParam(":detalle", $datos["detalle"], PDO::PARAM_STR);
		$stmt->bindParam(":pago", $datos["pago"], PDO::PARAM_STR);

		if($stmt->execute()){ 

			return "ok"; 

		}else{ 

			return "error"; 

		}

		$stmt->close();

		$tmt =null;
	}

	/*=============================================
	VERIFICAR PRODUCTO COMPRADO
	=============================================*/

	static public function mdlVerificarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_usuario = :id_usuario AND id_producto = :id_producto");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$tmt =null;

	}



	
	/*=============================================
	OBTENER CUPON INGRESADO
	=============================================*/

	static public function mdlObtenerCupon($tabla, $datos){

		if($datos !=null){
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE codigo = :codigo");
		
		    $stmt->bindParam(":codigo", $datos["codigoCupon"], PDO::PARAM_STR);
		}else{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ");

		}

		
		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt =null;

	}

	//
	/*=============================================
	PERSISTE CARRITO
	=============================================*/

	static public function mdlPersisteCarrito($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET carrito_json = :carrito_json, ultima_modificacion = sysdate() WHERE id_usuario = :id_usuario");
		
		//$datetime = date_create()->format('YYYY-mm-dd');//system date    H:i:s
		//$lastupdated = date('Y-m-d h:i:s');

		
		$stmt->bindParam(":carrito_json", $datos["carrito_json"], PDO::PARAM_STR);
		//$stmt->bindParam(":ultima_modificacion", $lastupdated, PDO::PARAM_STR);
		$stmt->bindParam(":id_usuario", $datos["usuarioCarrito"], PDO::PARAM_INT);
		
		

		if($stmt -> execute()){
			return "ok";
		}else{
			return "error";
		}
		$stmt-> close();
		$stmt = null;
	}


}