<?php

class Ruta{

	/*=============================================
	RUTA LADO DEL CLIENTE
	=============================================*/	

	public function ctrRuta(){

		return "https://localhost/frontendx/";
	
	}

	/*=============================================
	RUTA LADO DEL SERVIDOR
	=============================================*/	

	public function ctrRutaServidor(){

		return "https://localhost/backendx/";
	
	}

}