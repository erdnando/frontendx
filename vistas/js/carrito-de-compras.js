/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
VISUALIZAR LA CESTA DEL CARRITO DE COMPRAS
=============================================*/

if (localStorage.getItem("cantidadCesta") != null) {

    $(".cantidadCesta").html(localStorage.getItem("cantidadCesta"));
    $(".sumaCesta").html(localStorage.getItem("sumaCesta"));

} else {

    $(".cantidadCesta").html("0");
    $(".sumaCesta").html("0");
}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
VISUALIZAR LOS PRODUCTOS EN LA PÁGINA CARRITO DE COMPRAS
=============================================*/


if (localStorage.getItem("listaProductos") != null) {

    var listaCarrito = JSON.parse(localStorage.getItem("listaProductos"));

} else {

    $(".cuerpoCarrito").html('<div class="well">Aún no hay productos en el carrito de compras.</div>');
    $(".sumaCarrito").hide();
    $(".cabeceraCheckout").hide();
}

for (var i = 0; i < indice.length; i++) {
    if (indice[i] == "carrito-de-compras") {
        listaCarrito.forEach(funcionForEach);

        function funcionForEach(item, index) {
            var datosProducto = new FormData();
            var precio = 0;

            datosProducto.append("id", item.idProducto);

            $.ajax({
                url: rutaOculta + "ajax/producto.ajax.php",
                method: "POST",
                data: datosProducto,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(respuesta) {
                    if (respuesta["precioOferta"] == 0) {
                        precio = respuesta["precio"];
                    } else {
                        precio = respuesta["precioOferta"];
                    }
                    $(".cuerpoCarrito").append(
                        '<div clas="row itemCarrito">' +

                        '<div class="col-sm-1 col-xs-12">' +
                        //'<br>' +
                        '<center>' +
                        '<button class="btn btn-default backColor quitarItemCarrito" idProducto="' + item.idProducto + '" peso="' + item.peso + '">' +
                        '<i class="fa fa-times"></i>' +
                        '</button>' +
                        '</center>' +
                        '</div>' +

                        '<div class="col-sm-1 col-xs-12">' +
                        '<figure>' +
                        '<img style="margin-top:-4px;width:40px;height:40px" src="' + item.imagen + '" class="img-thumbnail">' +
                        '</figure>' +
                        '</div>' +
                        '<div class="col-sm-4 col-xs-12">' +
                        //'<br>' +
                        '<p class="tituloCarritoCompra text-left">' + item.titulo + '</p>' +
                        '</div>' +
                        '<div class="col-md-2 col-sm-1 col-xs-12">' +
                        //'<br>' +
                        '<p class="precioCarritoCompra text-center">MXN $<span>' + precio + '</span></p>' +
                        '</div>' +
                        '<div class="col-md-2 col-sm-3 col-xs-8">' +
                        //'<br>' +
                        '<div class="col-xs-8">' +
                        '<center>' +
                        '<input type="number" class="form-control cantidadItem" min="1" value="' + item.cantidad + '" tipo="' + item.tipo + '" precio="' + precio + '" idProducto="' + item.idProducto + '" item="' + index + '">' +
                        '</center>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-2 col-sm-1 col-xs-4 text-center">' +
                        //'<br>' +
                        '<p class="subTotal' + index + ' subtotales">' +
                        '<strong>MXN $<span>' + (Number(item.cantidad) * Number(precio)) + '</span></strong>' +
                        '</p>' +
                        '</div>' +
                        '</div>' +
                        '<div class="clearfix"></div>' +
                        '<hr style="margin-top:-6px">');
                    /*=============================================
                    EVITAR MANIPULAR LA CANTIDAD EN PRODUCTOS VIRTUALES
                    =============================================*/

                    $(".cantidadItem[tipo='virtual']").attr("readonly", "true");

                    // /*=============================================
                    // /*=============================================
                    // /*=============================================
                    // /*=============================================
                    // /*=============================================
                    // ACTUALIZAR SUBTOTAL
                    // =============================================*/
                    var precioCarritoCompra = $(".cuerpoCarrito .precioCarritoCompra span");

                    cestaCarrito(precioCarritoCompra.length);

                    sumaSubtotales();

                }

            })

        }

    }

}



/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
AGREGAR AL CARRITO
=============================================*/

$(".agregarCarrito").click(function() {

    var idProducto = $(this).attr("idProducto");
    var imagen = $(this).attr("imagen");
    var titulo = $(this).attr("titulo");
    var precio = $(this).attr("precio");
    var tipo = $(this).attr("tipo");
    var peso = $(this).attr("peso");

    var agregarAlCarrito = false;

    /*=============================================
    CAPTURAR DETALLES
    =============================================*/

    if (tipo == "virtual") {

        agregarAlCarrito = true;

    } else {

        var seleccionarDetalle = $(".seleccionarDetalle");

        for (var i = 0; i < seleccionarDetalle.length; i++) {

            console.log("seleccionarDetalle", $(seleccionarDetalle[i]).val());

            if ($(seleccionarDetalle[i]).val() == "") {

                swal({
                    title: "Debe seleccionar Talla y Color",
                    text: "",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "¡Seleccionar!",
                    closeOnConfirm: false
                })

                return;

            } else {

                titulo = titulo + "-" + $(seleccionarDetalle[i]).val();

                agregarAlCarrito = true;

            }

        }

    }

    /*=============================================
    ALMACENAR EN EL LOCALSTARGE LOS PRODUCTOS AGREGADOS AL CARRITO
    =============================================*/

    if (agregarAlCarrito) {

        /*=============================================
        RECUPERAR ALMACENAMIENTO DEL LOCALSTORAGE
        =============================================*/

        if (localStorage.getItem("listaProductos") == null) {

            listaCarrito = [];

        } else {

            var listaProductos = JSON.parse(localStorage.getItem("listaProductos"));

            for (var i = 0; i < listaProductos.length; i++) {

                if (listaProductos[i]["idProducto"] == idProducto && listaProductos[i]["tipo"] == "virtual") {

                    swal({
                        title: "El producto ya está agregado al carrito de compras",
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "¡Volver!",
                        closeOnConfirm: false
                    })

                    return;
                }
            }
            listaCarrito.concat(localStorage.getItem("listaProductos"));
        }

        listaCarrito.push({
            "idProducto": idProducto,
            "imagen": imagen,
            "titulo": titulo,
            "precio": precio,
            "tipo": tipo,
            "peso": peso,
            "cantidad": "1"
        });

        localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));
        //add cart and userid to DB xxx
        //console.log("Item added:250::");
        //console.log(JSON.stringify(listaCarrito));

        /*=============================================
        ACTUALIZAR LA CESTA
        =============================================*/

        var cantidadCesta = Number($(".cantidadCesta").html()) + 1;
        var sumaCesta = Number($(".sumaCesta").html()) + Number(precio);

        $(".cantidadCesta").html(cantidadCesta);
        $(".sumaCesta").html(sumaCesta);

        localStorage.setItem("cantidadCesta", cantidadCesta);
        localStorage.setItem("sumaCesta", sumaCesta);

        //Cartabandoned
        try {
            saveAbandonedCart(JSON.stringify(listaCarrito), localStorage.getItem("usuario"));
        } catch (error) {
            console.log("Error al persistir el carrito");
        }

        /*=============================================
        MOSTRAR ALERTA DE QUE EL PRODUCTO YA FUE AGREGADO
        =============================================*/

        swal({
                title: "",
                text: "¡Se ha agregado un nuevo producto al carrito de compras!",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "¡Continuar comprando!",
                confirmButtonText: "¡Ir a mi carrito de compras!",
                closeOnConfirm: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location = rutaOculta + "carrito-de-compras";
                }
            });

    }

})

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
QUITAR PRODUCTOS DEL CARRITO
=============================================*/

$(document).on("click", ".quitarItemCarrito", function() {

    $(this).parent().parent().parent().remove();

    var idProducto = $(".cuerpoCarrito button");
    var imagen = $(".cuerpoCarrito img");
    var titulo = $(".cuerpoCarrito .tituloCarritoCompra");
    var precio = $(".cuerpoCarrito .precioCarritoCompra span");
    var cantidad = $(".cuerpoCarrito .cantidadItem");

    /*=============================================
    SI AÚN QUEDAN PRODUCTOS VOLVERLOS AGREGAR AL CARRITO (LOCALSTORAGE)
    =============================================*/

    listaCarrito = [];

    if (idProducto.length != 0) {

        for (var i = 0; i < idProducto.length; i++) {

            var idProductoArray = $(idProducto[i]).attr("idProducto");
            var imagenArray = $(imagen[i]).attr("src");
            var tituloArray = $(titulo[i]).html();
            var precioArray = $(precio[i]).html();
            var pesoArray = $(idProducto[i]).attr("peso");
            var tipoArray = $(cantidad[i]).attr("tipo");
            var cantidadArray = $(cantidad[i]).val();

            listaCarrito.push({
                "idProducto": idProductoArray,
                "imagen": imagenArray,
                "titulo": tituloArray,
                "precio": precioArray,
                "tipo": tipoArray,
                "peso": pesoArray,
                "cantidad": cantidadArray
            });

        }

        localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));

        //update cart  xxx  date xxx
        //console.log("Item added:339::");
        //console.log(JSON.stringify(listaCarrito));

        sumaSubtotales();
        cestaCarrito(listaCarrito.length);

        //Cartabandoned
        try {
            saveAbandonedCart(JSON.stringify(listaCarrito), localStorage.getItem("usuario"));
        } catch (error) {
            console.log("Error al persistir el carrito");
        }


    } else {

        /*=============================================
        SI YA NO QUEDAN PRODUCTOS HAY QUE REMOVER TODO
        =============================================*/

        localStorage.removeItem("listaProductos");

        localStorage.setItem("cantidadCesta", "0");

        localStorage.setItem("sumaCesta", "0");

        $(".cantidadCesta").html("0");
        $(".sumaCesta").html("0");

        $(".cuerpoCarrito").html('<div class="well">Aún no hay productos en el carrito de compras.</div>');
        $(".sumaCarrito").hide();
        $(".cabeceraCheckout").hide();

        //Cartabandoned
        try {
            saveAbandonedCart(JSON.stringify(listaCarrito), localStorage.getItem("usuario"));
        } catch (error) {
            console.log("Error al persistir el carrito");
        }

    }

})


/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
GENERAR SUBTOTAL DESPUES DE CAMBIAR CANTIDAD
=============================================*/
$(document).on("change", ".cantidadItem", function() {

    var cantidad = $(this).val();
    var precio = $(this).attr("precio");
    var idProducto = $(this).attr("idProducto");
    var item = $(this).attr("item");

    $(".subTotal" + item).html('<strong>MXN $<span>' + (cantidad * precio) + '</span></strong>');

    /*=============================================
    ACTUALIZAR LA CANTIDAD EN EL LOCALSTORAGE
    =============================================*/

    var idProducto = $(".cuerpoCarrito button");
    var imagen = $(".cuerpoCarrito img");
    var titulo = $(".cuerpoCarrito .tituloCarritoCompra");
    var precio = $(".cuerpoCarrito .precioCarritoCompra span");
    var cantidad = $(".cuerpoCarrito .cantidadItem");

    listaCarrito = [];

    for (var i = 0; i < idProducto.length; i++) {

        var idProductoArray = $(idProducto[i]).attr("idProducto");
        var imagenArray = $(imagen[i]).attr("src");
        var tituloArray = $(titulo[i]).html();
        var precioArray = $(precio[i]).html();
        var pesoArray = $(idProducto[i]).attr("peso");
        var tipoArray = $(cantidad[i]).attr("tipo");
        var cantidadArray = $(cantidad[i]).val();

        listaCarrito.push({
            "idProducto": idProductoArray,
            "imagen": imagenArray,
            "titulo": tituloArray,
            "precio": precioArray,
            "tipo": tipoArray,
            "peso": pesoArray,
            "cantidad": cantidadArray
        });

    }


    localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));

    sumaSubtotales();
    cestaCarrito(listaCarrito.length);
})

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
SUMA DE TODOS LOS SUBTOTALES
=============================================*/
function sumaSubtotales() {

    var subtotales = $(".subtotales span");
    var arraySumaSubtotales = [];

    for (var i = 0; i < subtotales.length; i++) {

        var subtotalesArray = $(subtotales[i]).html();
        arraySumaSubtotales.push(Number(subtotalesArray));

    }


    function sumaArraySubtotales(total, numero) {

        return total + numero;

    }

    var sumaTotal = arraySumaSubtotales.reduce(sumaArraySubtotales);

    $(".sumaSubTotal").html('<strong>MXN $<span>' + (sumaTotal).toFixed(2) + '</span></strong>');

    $(".sumaCesta").html((sumaTotal).toFixed(2));

    localStorage.setItem("sumaCesta", (sumaTotal).toFixed(2));


}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
ACTUALIZAR CESTA AL CAMBIAR CANTIDAD
=============================================*/
function cestaCarrito(cantidadProductos) {

    /*=============================================
    SI HAY PRODUCTOS EN EL CARRITO
    =============================================*/

    if (cantidadProductos != 0) {

        var cantidadItem = $(".cuerpoCarrito .cantidadItem");

        var arraySumaCantidades = [];

        for (var i = 0; i < cantidadItem.length; i++) {

            var cantidadItemArray = $(cantidadItem[i]).val();
            arraySumaCantidades.push(Number(cantidadItemArray));

        }

        function sumaArrayCantidades(total, numero) {

            return total + numero;

        }

        var sumaTotalCantidades = arraySumaCantidades.reduce(sumaArrayCantidades);

        $(".cantidadCesta").html(sumaTotalCantidades);
        localStorage.setItem("cantidadCesta", sumaTotalCantidades);

    }

}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
CHECKOUT
=============================================*/
// Realizar pago button
$("#btnCheckout").click(function() {

    //limpia tabla de productos
    $(".listaProductos table.tablaProductos tbody").html("");

    //$("#checkPaypal").prop("checked", true);
    //$("#checkPayu").prop("checked", false);

    var idUsuario = $(this).attr("idUsuario");
    var peso = $(".cuerpoCarrito button, .comprarAhora button");
    var titulo = $(".cuerpoCarrito .tituloCarritoCompra, .comprarAhora .tituloCarritoCompra");
    var cantidad = $(".cuerpoCarrito .cantidadItem, .comprarAhora .cantidadItem");
    var subtotal = $(".cuerpoCarrito .subtotales span, .comprarAhora .subtotales span");
    var tipoArray = [];
    var cantidadPeso = [];

    /*=============================================
    SUMA SUBTOTAL
    =============================================*/

    var sumaSubTotal = $(".sumaSubTotal span")

    $(".valorSubtotal").html($(sumaSubTotal).html());
    $(".valorSubtotal").attr("valor", $(sumaSubTotal).html());

    /*=============================================
    TASAS DE IMPUESTO
    =============================================*/

    var impuestoTotal = ($(".valorSubtotal").html() * $("#tasaImpuesto").val()) / 100;

    $(".valorTotalImpuesto").html((impuestoTotal).toFixed(2));
    $(".valorTotalImpuesto").attr("valor", (impuestoTotal).toFixed(2));

    sumaTotalCompra()

    /*=============================================
    VARIABLES ARRAY 
    =============================================*/

    for (var i = 0; i < titulo.length; i++) {

        var pesoArray = $(peso[i]).attr("peso");
        var tituloArray = $(titulo[i]).html();
        var cantidadArray = $(cantidad[i]).val();
        var subtotalArray = $(subtotal[i]).html();

        /*=============================================
        EVALUAR EL PESO DE ACUERDO A LA CANTIDAD DE PRODUCTOS
        =============================================*/

        cantidadPeso[i] = pesoArray * cantidadArray;

        function sumaArrayPeso(total, numero) {

            return total + numero;

        }

        var sumaTotalPeso = cantidadPeso.reduce(sumaArrayPeso);

        /*=============================================
        MOSTRAR PRODUCTOS DEFINITIVOS A COMPRAR
        =============================================*/

        $(".listaProductos table.tablaProductos tbody").append('<tr>' +
            '<td class="valorTitulo">' + tituloArray + '</td>' +
            '<td class="valorCantidad">' + cantidadArray + '</td>' +
            '<td>$<span class="valorItem" valor="' + subtotalArray + '">' + subtotalArray + '</span></td>' +
            '<tr>');

        /*=============================================
        SELECCIONAR PAÍS DE ENVÍO SI HAY PRODUCTOS FÍSICOS
        =============================================*/

        tipoArray.push($(cantidad[i]).attr("tipo"));

        function checkTipo(tipo) {

            return tipo == "fisico";

        }

    }


    /*=============================================
    OBTIENE COSTO ENVIO
    =============================================*/
    var resultadoPeso = sumaTotalPeso * $("#envioNacional").val();

    console.log("sumaTotalPeso::" + sumaTotalPeso);
    console.log("envioNacional::" + $("#envioNacional").val());
    console.log("resultadoPeso::" + resultadoPeso);
    console.log("tasaMinimaNal::" + $("#tasaMinimaNal").val());


    if (resultadoPeso < $("#tasaMinimaNal").val()) {
        $(".valorTotalEnvio").html($("#tasaMinimaNal").val());
        $(".valorTotalEnvio").attr("valor", $("#tasaMinimaNal").val());
    } else {
        $(".valorTotalEnvio").html(resultadoPeso);
        $(".valorTotalEnvio").attr("valor", resultadoPeso);
    }

    sumaTotalCompra();


    /*=============================================
    EXISTEN PRODUCTOS FÍSICOS
    =============================================*/

    // if (tipoArray.find(checkTipo) == "fisico") {

    //$(".formEnvio").show();
    $(".btnPagar").attr("tipo", "fisico");

    $('.descuentoAplicado').attr('style', 'display:none');

    //inicializa forma de pago
    $('#seccionCVC').attr('style', 'display: none');
    $('#pagoCVC').val('');


    //inicializa cupon
    $('#seccionCupon').attr('style', 'display: none');
    $('#pagocupon').val('');


    $('.btnPagar').prop('disabled', true);
    $('.btnAplicarCupon').prop('disabled', true);


    var datosDom = new FormData();
    var idUsuario = localStorage.getItem("usuario");
    datosDom.append("id", idUsuario);

    $.ajax({
        url: rutaOculta + "ajax/domicilios.ajax.php",
        method: "POST",
        data: datosDom,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {

            //llenar combo direcciones
            console.log(respuesta);
            $("#cboDirecciones").empty();
            //$("#cboDirecciones").selectpicker("refresh");
            $("#cboDirecciones").append("<option value='0' data-content='<strong>Seleccionar la dirección de entrega</strong>'");
            $("#cboDirecciones").selectpicker("refresh");

            respuesta.forEach(cargaDirecciones);

            function cargaDirecciones(item, index) {

                $("#cboDirecciones").append("<option value=" + (item.id) + " data-content='<div><h4>" + item.calle + "</h4><h5>" + item.cp + "," + item["municipio-colonia"] + "</h5><h5>" + item.estado + "," + item.pais + "</h5></div>'></option>");
                $("#cboDirecciones").selectpicker("refresh");
            }

            $("#cboDirecciones").append("<option value='-1' data-content='<strong>Agregar otra dirección</strong>'");
            $("#cboDirecciones").selectpicker("refresh");
        },
        error: function(error) {
            console.log(error);
        }

    });

    //-----------------------------------

    var datosFormasPago = new FormData();
    //var idUsuario = localStorage.getItem("usuario");
    datosFormasPago.append("id", idUsuario);

    $.ajax({
        url: rutaOculta + "ajax/formaspago.ajax.php",
        method: "POST",
        data: datosFormasPago,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {

            //llenar combo formas pago
            console.log(respuesta);

            $("#cboFormaspago").empty();
            //$("#cboDirecciones").selectpicker("refresh");
            $("#cboFormaspago").append("<option value='0' data-content='<strong>Seleccionar la forma de pago</strong>'");
            $("#cboFormaspago").selectpicker("refresh");
            respuesta.forEach(cargaFormasPago);

            function cargaFormasPago(item, index) {

                var tarjeta = item.cuenta.replace(/\d(?=\d{4})/g, "*");

                $("#cboFormaspago").append("<option value=" + (item.id) + " data-content='<div><h4>" + tarjeta + "</h4><h5>" + item.nombre + "</h5><h5>" + item.fechacaducidad + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + item.tipo + "</h5></div>'></option>");
                $("#cboFormaspago").selectpicker("refresh");
            }

            $("#cboFormaspago").append("<option value='-1' data-content='<strong>Agregar otra forma de pago</strong>'");
            $("#cboFormaspago").selectpicker("refresh");
        },
        error: function(error) {
            console.log(error);
        }

    });


    /*=============================================
    VALIDA DIRECCION SELECCIONADA
    =============================================*/
    $("#cboDirecciones").change(function() {

        console.log($("#cboDirecciones").val());

        //-1 Agrega nueva dirección
        if ($("#cboDirecciones").val() == "-1") {
            window.location = rutaOculta + "index.php?ruta=perfil&tab=4";
        }

        validarformapago();

    })

    /*=============================================
    VALIDA FORMA DE PAGO SELECCIONADO
    =============================================*/
    $("#cboFormaspago").change(function() {

        console.log($("#cboFormaspago").val());

        //-1 Agrega nueva dirección
        if ($("#cboFormaspago").val() == "-1") {
            window.location = rutaOculta + "index.php?ruta=perfil&tab=5";
        }

        if ($("#cboFormaspago").val() != 0) {
            $('#seccionCVC').attr('style', 'display: block');
        } else {
            $('#seccionCVC').attr('style', 'display: none');
        }


        if ($("#cboFormaspago").val() != 0) {
            $('#seccionCupon').attr('style', 'display: block');
        } else {
            $('#seccionCupon').attr('style', 'display: none');
        }

        validarformapago();
    })

    /*=============================================
    EVALUAR TASAS DE ENVÍO SI EL PRODUCTO ES FÍSICO
    =============================================*/

    /*$("#seleccionarPais").change(function() {

        $(".alert").remove();

        var pais = $(this).val();
        var tasaPais = $("#tasaPais").val();

        if (pais == tasaPais) {

            var resultadoPeso = sumaTotalPeso * $("#envioNacional").val();

            if (resultadoPeso < $("#tasaMinimaNal").val()) {

                $(".valorTotalEnvio").html($("#tasaMinimaNal").val());
                $(".valorTotalEnvio").attr("valor", $("#tasaMinimaNal").val());

            } else {

                $(".valorTotalEnvio").html(resultadoPeso);
                $(".valorTotalEnvio").attr("valor", resultadoPeso);
            }

        } else {

            var resultadoPeso = sumaTotalPeso * $("#envioInternacional").val();

            if (resultadoPeso < $("#tasaMinimaInt").val()) {

                $(".valorTotalEnvio").html($("#tasaMinimaInt").val());
                $(".valorTotalEnvio").attr("valor", $("#tasaMinimaInt").val());

            } else {

                $(".valorTotalEnvio").html(resultadoPeso);
                $(".valorTotalEnvio").attr("valor", resultadoPeso);
            }

        }

        sumaTotalCompra();
        pagarConPayu();

    })*/

    // } else {

    //     $(".btnPagar").attr("tipo", "virtual");
    // }

})

function justNumbersCVC(evt) {
    var theEvent = evt || window.event;

    if ($("#pagoCVC").val().trim().length > 2) {
        $('.btnPagar').prop('disabled', false);
    } else {
        $('.btnPagar').prop('disabled', true);
    }

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }

    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    } else {
        validarformapago();
    }
}


function justAlfaNumericoCuponUP(evt) {

    var theEvent = evt || window.event;

    //console.log($("#pagocupon").val().trim().length);
    if ($("#pagocupon").val().trim().length > 2) {
        $('.btnAplicarCupon').prop('disabled', false);
    } else {
        $('.btnAplicarCupon').prop('disabled', true);
    }


}

function justAlfaNumericoCupon(evt) {
    var theEvent = evt || window.event;

    //console.log($("#pagocupon").val().trim().length);
    if ($("#pagocupon").val().trim().length > 2) {
        $('.btnAplicarCupon').prop('disabled', false);
    } else {
        $('.btnAplicarCupon').prop('disabled', true);
    }

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }

    var regex = /[0-9A-Za-z-]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    } else {
        validarformapago();
    }




}


function validarformapago() {
    $('.btnPagar').prop('disabled', true);

    if ($("#cboDirecciones").val() != 0 && $("#cboDirecciones").val() != -1) {
        if ($("#cboFormaspago").val() != 0 && $("#cboFormaspago").val() != -1) {
            if ($("#pagoCVC").val() != "") {
                $('.btnPagar').prop('disabled', false);
            }
        }
    }


}


function saveAbandonedCart(jsonCart, idUsuario) {

    var datos = new FormData();
    console.log(jsonCart);
    console.log(idUsuario);

    datos.append("carrito_json", jsonCart);
    datos.append("usuarioCarrito", idUsuario);


    $.ajax({
        url: rutaOculta + "ajax/carrito.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {

            console.log(respuesta);
            //var response = JSON.parse(respuesta);



        }

    })


}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
SUMA TOTAL DE LA COMPRA
=============================================*/
function sumaTotalCompra() {

    var sumaTotalTasas = Number($(".valorSubtotal").html()) +
        Number($(".valorTotalEnvio").html()) +
        Number($(".valorTotalImpuesto").html());


    $(".valorTotalCompra").html((sumaTotalTasas).toFixed(2));
    $(".valorTotalCompra").attr("valor", (sumaTotalTasas).toFixed(2));

    localStorage.setItem("total", hex_md5($(".valorTotalCompra").html()));
}

/*SUMA TOTAL DE LA COMPRA CON DESCUENTO DLE CUPON
=============================================*/
function sumaTotalCompraConDescuento(_descuento) {

    var sumaTotalTasas = Number($(".valorSubtotal").html()) +
        Number($(".valorTotalEnvio").html()) +
        Number($(".valorTotalImpuesto").html());


    $(".valorTotalCompra").html((Number(sumaTotalTasas) - Number(_descuento)).toFixed(2));
    $(".valorTotalCompra").attr("valor", (Number(sumaTotalTasas) - Number(_descuento)).toFixed(2));

    localStorage.setItem("total", hex_md5($(".valorTotalCompra").html()));
}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
MÉTODO DE PAGO PARA CAMBIO DE DIVISA
=============================================*/

var metodoPago = "paypal";
divisas(metodoPago);

$("input[name='pago']").change(function() {

    var metodoPago = $(this).val();

    divisas(metodoPago);

    if (metodoPago == "payu") {

        $(".btnPagar").hide();
        $(".formPayu").show();

        pagarConPayu();

    } else {

        $(".btnPagar").show();
        $(".formPayu").hide();

    }

})

/*=============================================
/*=============================================
/*=============================================
/*=============================================
FUNCIÓN PARA EL CAMBIO DE DIVISA
=============================================*/

function divisas(metodoPago) {

    $("#cambiarDivisa").html("");

    if (metodoPago == "paypal") {

        /* $("#cambiarDivisa").append('<option value="USD">USD</option>' +
            '<option value="EUR">EUR</option>' +
            '<option value="GBP">GBP</option>' +
            '<option value="MXN">MXN</option>' +
            '<option value="JPY">JPY</option>' +
            '<option value="CAD">CAD</option>' +
			'<option value="BRL">BRL</option>')*/
        $("#cambiarDivisa").append('<option value="MXN">MXN</option>' +
            '<option value="USD">USD</option>')

    } else {

        $("#cambiarDivisa").append('<option value="USD">USD</option>' +
            '<option value="PEN">PEN</option>' +
            '<option value="COP">COP</option>' +
            '<option value="MXN">MXN</option>' +
            '<option value="CLP">CLP</option>' +
            '<option value="ARS">ARS</option>' +
            '<option value="BRL">BRL</option>')

    }

}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
CAMBIO DE DIVISA
=============================================*/

var divisaBase = "MXN";

/*$("#cambiarDivisa").change(function() {

    $(".alert").remove();

    if ($("#seleccionarPais").val() == "") {

        $("#cambiarDivisa").after('<div class="alert alert-warning">No ha seleccionado el país de envío</div>');

        return;

    }

    var divisa = $(this).val();

    $.ajax({

        url: "https://free.currconv.com/api/v7/convert?q=" + divisaBase + "_" + divisa + "&compact=ultra&apiKey=a01ebaf9a1c69eb4ff79",
        type: "GET",
        cache: false,
        contentType: false,
        processData: false,
        dataType: "jsonp",
        success: function(respuesta) {

            var conversion = (respuesta["MXN_" + divisa]).toFixed(2);

            $(".cambioDivisa").html(divisa);

            if (divisa == "MXN") {

                $(".valorSubtotal").html($(".valorSubtotal").attr("valor"))
                $(".valorTotalEnvio").html($(".valorTotalEnvio").attr("valor"))
                $(".valorTotalImpuesto").html($(".valorTotalImpuesto").attr("valor"))
                $(".valorTotalCompra").html($(".valorTotalCompra").attr("valor"))

                var valorItem = $(".valorItem");

                localStorage.setItem("total", hex_md5($(".valorTotalCompra").html()));

                for (var i = 0; i < valorItem.length; i++) {

                    $(valorItem[i]).html($(valorItem[i]).attr("valor"));

                }

            } else {

                $(".valorSubtotal").html(

                    Math.ceil(Number(conversion) * Number($(".valorSubtotal").attr("valor")) * 100) / 100

                )

                $(".valorTotalEnvio").html(

                    (Number(conversion) * Number($(".valorTotalEnvio").attr("valor"))).toFixed(2)

                )

                $(".valorTotalImpuesto").html(

                    (Number(conversion) * Number($(".valorTotalImpuesto").attr("valor"))).toFixed(2)

                )

                $(".valorTotalCompra").html(

                    (Number(conversion) * Number($(".valorTotalCompra").attr("valor"))).toFixed(2)

                )

                var valorItem = $(".valorItem");

                localStorage.setItem("total", hex_md5($(".valorTotalCompra").html()));

                for (var i = 0; i < valorItem.length; i++) {

                    $(valorItem[i]).html(

                        (Number(conversion) * Number($(valorItem[i]).attr("valor"))).toFixed(2)

                    );

                }

            }

            sumaTotalCompra();

            pagarConPayu();

        }

    })



})*/

//BOTON APLICAR CUPON
//--------------------------------------
$(".btnAplicarCupon").click(function() {
        $("#hdnDescuento").val(0)
        $('.descuentoAplicado').attr('style', 'display: none');
        if ($("#pagocupon").val().trim() == "") {
            swal({
                title: "!Ingrese un cupón!",
                text: "",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Regresar",
                closeOnConfirm: false
            })
            return;
        }

        var datos = new FormData();
        $_cupon = $("#pagocupon").val().trim().toUpperCase();
        datos.append("codigoCupon", $_cupon);


        $.ajax({
            url: rutaOculta + "ajax/carrito.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {

                console.log(JSON.parse(respuesta));
                var response = JSON.parse(respuesta);
                //Cuando no hay monto. En caso de que no haya por alguna extraña razón

                if (Number($(".valorSubtotal").html() <= 0)) {
                    swal({
                        title: "!Para aplicar el cupón debe tener un monto!",
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Regresar",
                        closeOnConfirm: false
                    })

                    $("#pagocupon").val("");
                    sumaTotalCompraConDescuento(0);
                    return;
                }

                //cuando el cupon no existe
                if (response == false) {
                    swal({
                        title: "!El cupón no existe, favor de validar!",
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Regresar",
                        closeOnConfirm: false
                    })

                    $("#pagocupon").val("");
                    sumaTotalCompraConDescuento(0);
                    return;
                }
                //cuando ya no hay cupones disponibles de esta promocion
                if (parseInt(response["disponibles"]) <= 0) {
                    swal({
                        title: "!Lo sentimos, el cupón ha dejado de estar disponible!",
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Regresar",
                        closeOnConfirm: false
                    })

                    $("#pagocupon").val("");
                    sumaTotalCompraConDescuento(0);
                    return;
                }

                //cuando ya no hay cupones disponibles de esta promocion (1=activo, 0=inactivo)
                if (response["estatus"] == "0") {
                    swal({
                        title: "!Lo sentimos, el cupón ya no está activo!",
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Regresar",
                        closeOnConfirm: false
                    })
                    $("#pagocupon").val("");
                    sumaTotalCompraConDescuento(0);

                    return;
                }

                //cuando ya caduco la vigencia del cupón)
                if (response["vigencia"] != null) {
                    //console.log(response["vigencia"]);
                    try {
                        var caduca = new Date(response["vigencia"]);
                        caduca.setDate(caduca.getDate() + 1);

                        var hoy = new Date();
                        /* console.log(caduca);
                         console.log(hoy);
                         console.log(caduca < hoy);*/

                        if ((caduca < hoy)) {
                            //caduco
                            swal({
                                title: "!Lo sentimos, el cupón ha caducado. Lo invitamos a estar pendiente por las nuevas promociones!",
                                text: "",
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Regresar",
                                closeOnConfirm: false
                            })
                            $("#pagocupon").val("");
                            sumaTotalCompraConDescuento(0);
                            return;

                        }
                    } catch (error) {
                        console.log("Error al leer la vigencia del cupón");
                        sumaTotalCompraConDescuento(0);
                        return;
                    }


                }

                //recupera el descuento para aplicarlo
                var descuentoCupon = response["descuento"];
                var tipoDescuento = response["tipo"]; //1->$, 2->%
                var descuentoAplicado = 0;

                //descuenta $$
                if (tipoDescuento == "1") {
                    descuentoAplicado = Number($(".valorSubtotal").html()) - descuentoCupon;
                } else { //descuenta por %%
                    descuentoAplicado = Number($(".valorSubtotal").html()) * Number(descuentoCupon) / 100;
                }

                console.log(descuentoAplicado);

                if (descuentoCupon >= Number($(".valorSubtotal").html()) && tipoDescuento == "1") {
                    swal({
                        title: "!Lo sentimos, el cupón no es aplicable para este monto!",
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Regresar",
                        closeOnConfirm: false
                    })

                    $("#pagocupon").val("");
                    sumaTotalCompraConDescuento(0);

                    return;
                }
                //recalcula los totales
                sumaTotalCompraConDescuento(descuentoAplicado);

                //pintar el ahorraste
                $('.descuentoAplicado').attr('style', 'border-radius: 5px;text-align: center;margin-top:-20px;margin-left: 8px;display: block;color: white;background: red');
                $(".descuentoAplicado").html("Descuento por cupón $" + descuentoAplicado);
                console.log("descuento aplicado");
                $("#hdnDescuento").val(descuentoAplicado)


            }

        })


    })
    /*=============================================
    /*=============================================
    /*=============================================
    /*=============================================
    /*=============================================
    BOTÓN PAGAR
    =============================================*/

$(".btnPagar").click(function() {
    var bContinue = false;
    //;
    // console.log($(this).attr("tipo")); //fisico
    // console.log($("#seleccionarPais").val()); //undefined
    // console.log($("#cambiarDivisa").val()); //undefined
    // console.log($(".valorTotalCompra").html()); //1445.98
    // console.log(localStorage.getItem("total")); //total encriptado
    // console.log($(".valorTotalImpuesto").html()); //70.48
    // console.log($(".valorTotalEnvio").html()); //935
    // console.log($(".valorSubtotal").html()); //440.50
    // console.log($(".valorTitulo"));
    // console.log($(".valorCantidad"));
    // console.log($(".valorItem"));
    // console.log($('.cuerpoCarrito button, .comprarAhora button'));



    if ($("#cboDirecciones").val() != 0 && $("#cboDirecciones").val() != -1) {
        if ($("#cboFormaspago").val() != 0 && $("#cboFormaspago").val() != -1) {
            if ($("#pagoCVC").val() != "") {
                bContinue = true;
            }
        }
    }


    if (!bContinue) {
        swal({
            title: "¡Asegurese de haber ingresado una dirección de entrega, forma de pago y su CVC!",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Regresar",
            closeOnConfirm: false
        })
        return;
    }

    var tipo = $(this).attr("tipo");

    // if (tipo == "fisico" && $("#seleccionarPais").val() == "") {
    //     $(".btnPagar").after('<div class="alert alert-warning">No ha seleccionado el país de envío</div>');
    //     return;
    // }

    var divisa = $("#cambiarDivisa").val();
    var total = $(".valorTotalCompra").html();
    var totalEncriptado = localStorage.getItem("total");
    var impuesto = $(".valorTotalImpuesto").html();
    var envio = $(".valorTotalEnvio").html();
    var subtotal = $(".valorSubtotal").html();
    var descuento = $("#hdnDescuento").val()
    var titulo = $(".valorTitulo");
    var cantidad = $(".valorCantidad");
    var valorItem = $(".valorItem");
    var idProducto = $('.cuerpoCarrito button, .comprarAhora button');

    var cvc = $("#pagoCVC").val();
    var idDireccion = $("#cboDirecciones").val();
    var idFormapago = $("#cboFormaspago").val();

    var tituloArray = [];
    var cantidadArray = [];
    var valorItemArray = [];
    var idProductoArray = [];

    for (var i = 0; i < titulo.length; i++) {

        tituloArray[i] = $(titulo[i]).html();
        cantidadArray[i] = $(cantidad[i]).html();
        valorItemArray[i] = $(valorItem[i]).html();
        idProductoArray[i] = $(idProducto[i]).attr("idProducto");

    }

    var datos = new FormData();

    datos.append("cvc", cvc);
    datos.append("idDireccion", idDireccion);
    datos.append("idFormapago", idFormapago);
    datos.append("total", total);
    datos.append("descuento", descuento);
    datos.append("totalEncriptado", totalEncriptado);
    datos.append("impuesto", impuesto);
    datos.append("envio", envio);
    datos.append("subtotal", subtotal);
    datos.append("tituloArray", tituloArray);
    datos.append("cantidadArray", cantidadArray);
    datos.append("valorItemArray", valorItemArray);
    datos.append("idProductoArray", idProductoArray);

    console.log("datos:::::::::.");
    console.log(...datos);
    // $.ajax({
    //     url: rutaOculta + "ajax/carrito.ajax.php",
    //     method: "POST",
    //     data: datos,
    //     cache: false,
    //     contentType: false,
    //     processData: false,
    //     success: function(respuesta) {

    //         window.location = respuesta;

    //     }

    // })

})

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
BOTÓN PAGAR PAYU
=============================================*/

function pagarConPayu() {

    if ($("#seleccionarPais").val() == "") {

        $(".formPayu").after('<div class="alert alert-warning">No ha seleccionado el país de envío</div>');

        $(".formPayu input[name='Submit']").attr("type", "button");

        return;

    }

    var divisa = $("#cambiarDivisa").val();
    var total = $(".valorTotalCompra").html();
    var impuesto = $(".valorTotalImpuesto").html();
    var envio = $(".valorTotalEnvio").html();
    var subtotal = $(".valorSubtotal").html();
    var titulo = $(".valorTitulo");
    var cantidad = $(".valorCantidad");
    var valorItem = $(".valorItem");
    var idProducto = $('.cuerpoCarrito button, .comprarAhora button');

    var tituloArray = [];
    var cantidadArray = [];
    var idProductoArray = [];
    var valorItemArray = [];

    for (var i = 0; i < titulo.length; i++) {

        tituloArray[i] = $(titulo[i]).html();
        cantidadArray[i] = $(cantidad[i]).html();
        idProductoArray[i] = $(idProducto[i]).attr("idProducto");
        valorItemArray[i] = $(valorItem[i]).html();

    }

    var valorItemString = valorItemArray.toString();
    var pago = valorItemString.replace(",", "-");

    var datos = new FormData();
    datos.append("metodoPago", "payu");
    datos.append("cantidadArray", cantidadArray);
    datos.append("valorItemArray", valorItemArray);
    datos.append("idProductoArray", idProductoArray);
    datos.append("divisaPayu", divisa);

    if (hex_md5(total) == localStorage.getItem("total")) {

        $.ajax({
            url: rutaOculta + "ajax/carrito.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {

                var merchantId = JSON.parse(respuesta).merchantIdPayu;
                var accountId = JSON.parse(respuesta).accountIdPayu;
                var apiKey = JSON.parse(respuesta).apiKeyPayu;
                var modo = JSON.parse(respuesta).modoPayu;
                var description = tituloArray.toString();
                var referenceCode = (Number(Math.ceil(Math.random() * 1000000)) + Number(total).toFixed());
                var productosToString = idProductoArray.toString();
                var productos = productosToString.replace(/,/g, "-");
                var cantidadToString = cantidadArray.toString();
                var cantidad = cantidadToString.replace(/,/g, "-");
                var signature = hex_md5(apiKey + "~" + merchantId + "~" + referenceCode + "~" + total + "~" + divisa);


                if (divisa == "COP") {

                    var taxReturnBase = (total - impuesto).toFixed(2)

                } else {

                    var taxReturnBase = 0;

                }

                if (modo == "sandbox") {

                    var url = "https://sandbox.gateway.payulatam.com/ppp-web-gateway/";
                    var test = 1;

                } else {

                    var url = "https://gateway.payulatam.com/ppp-web-gateway/";
                    var test = 0;

                }

                if (envio != 0) {

                    var tipoEnvio = "YES";

                } else {

                    var tipoEnvio = "NO";
                }

                $(".formPayu").attr("method", "POST");
                $(".formPayu").attr("action", url);
                $(".formPayu input[name='merchantId']").attr("value", merchantId);
                $(".formPayu input[name='accountId']").attr("value", accountId);
                $(".formPayu input[name='description']").attr("value", description);
                $(".formPayu input[name='referenceCode']").attr("value", referenceCode);
                $(".formPayu input[name='amount']").attr("value", total);
                $(".formPayu input[name='tax']").attr("value", impuesto);
                $(".formPayu input[name='taxReturnBase']").attr("value", taxReturnBase);
                $(".formPayu input[name='shipmentValue']").attr("value", envio);
                $(".formPayu input[name='currency']").attr("value", divisa);
                $(".formPayu input[name='responseUrl']").attr("value", rutaOculta + "index.php?ruta=finalizar-compra&payu=true&productos=" + productos + "&cantidad=" + cantidad + "&pago=" + pago);
                $(".formPayu input[name='declinedResponseUrl']").attr("value", rutaOculta + "carrito-de-compras");
                $(".formPayu input[name='displayShippingInformation']").attr("value", tipoEnvio);
                $(".formPayu input[name='test']").attr("value", test);
                $(".formPayu input[name='signature']").attr("value", signature);

                /*=============================================
				GENERADOR DE TARJETAS DE CRÉDITO
				http://www.elfqrin.com/discard_credit_card_generator.php
				=============================================*/

            }

        })
    }
}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
AGREGAR PRODUCTOS GRATIS
=============================================*/
$(".agregarGratis").click(function() {

    var idProducto = $(this).attr("idProducto");
    var idUsuario = $(this).attr("idUsuario");
    var tipo = $(this).attr("tipo");
    var titulo = $(this).attr("titulo");
    var agregarGratis = false;

    /*=============================================
    VERIFICAR QUE NO TENGA EL PRODUCTO ADQUIRIDO
    =============================================*/

    var datos = new FormData();

    datos.append("idUsuario", idUsuario);
    datos.append("idProducto", idProducto);

    $.ajax({
        url: rutaOculta + "ajax/carrito.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {

            if (respuesta != "false") {

                swal({
                    title: "¡Usted ya adquirió este producto!",
                    text: "",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Regresar",
                    closeOnConfirm: false
                })


            } else {

                if (tipo == "virtual") {

                    agregarGratis = true;

                } else {

                    var seleccionarDetalle = $(".seleccionarDetalle");

                    for (var i = 0; i < seleccionarDetalle.length; i++) {

                        if ($(seleccionarDetalle[i]).val() == "") {

                            swal({
                                title: "Debe seleccionar Talla y Color",
                                text: "",
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "¡Seleccionar!",
                                closeOnConfirm: false
                            })

                        } else {

                            titulo = titulo + "-" + $(seleccionarDetalle[i]).val();

                            agregarGratis = true;

                        }

                    }

                }

                if (agregarGratis) {

                    window.location = rutaOculta + "index.php?ruta=finalizar-compra&gratis=true&producto=" + idProducto + "&titulo=" + titulo;

                }

            }

        }

    })


})