$(document).ready(function() {

    var tabSeleccionado = getParameterByName("tab");

    console.log(tabSeleccionado);
    if (tabSeleccionado == 4)
        $('#tabDirecciones').trigger('click');
    else if (tabSeleccionado == 5)
        $('#tabFormasPago').trigger('click');
});



/*=============================================
CAPTURA DE RUTA
=============================================*/

var rutaActual = location.href;

$(".btnIngreso, .facebook, .google").click(function() {
    localStorage.setItem("rutaActual", rutaActual);
})

/*=============================================
FORMATEAR LOS IPUNT
=============================================*/

$("input").focus(function() {

    $(".alert").remove();
})

/*=============================================
VALIDAR EMAIL REPETIDO
=============================================*/

var validarEmailRepetido = false;

$("#regEmail").change(function() {

    var email = $("#regEmail").val();

    var datos = new FormData();
    datos.append("validarEmail", email);

    $.ajax({

        url: rutaOculta + "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {

            if (respuesta == "false") {

                $(".alert").remove();
                validarEmailRepetido = false;

            } else {

                var modo = JSON.parse(respuesta).modo;

                if (modo == "directo") {

                    modo = "esta página";
                }

                $("#regEmail").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> El correo electrónico ya existe en la base de datos, fue registrado a través de ' + modo + ', por favor ingrese otro diferente</div>')

                validarEmailRepetido = true;

            }

        }

    })

})


$(function() {
    $('#frmRegistro').submit(function() {
        //$('#loaderImg').show();
        $('#loaderImg').css({ 'display': 'block' });
        return true;
    });
});

$(function() {
    $('#passEmailxxxx').blur(function() {

        swal({
                title: "¡Gracias utilizar el servicio de nueva contraseña!",
                text: "Validando y enviando correo ...",
                type: "info",
                confirmButtonText: "Espere",
                closeOnConfirm: false
            },

            function(isConfirm) {
                if (isConfirm) {

                }
            });


        var datos = new FormData();
        datos.append("passEmail", "1");


        $.ajax({
            url: rutaOculta + "ajax/usuarios.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {
                return true;
            }
        })

    });
});



/*=============================================
VALIDAR EL REGISTRO DE USUARIO
=============================================*/
function registroUsuario() {

    /*=============================================
    VALIDAR EL NOMBRE
    =============================================*/

    var nombre = $("#regUsuario").val();

    if (nombre != "") {

        var expresion = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/;

        if (!expresion.test(nombre)) {

            $("#regUsuario").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> No se permiten números ni caracteres especiales</div>')

            return false;

        }

    } else {

        $("#regUsuario").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Este campo es obligatorio</div>')

        return false;
    }

    /*=============================================
    VALIDAR EL EMAIL
    =============================================*/

    var email = $("#regEmail").val();

    if (email != "") {

        var expresion = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

        if (!expresion.test(email)) {

            $("#regEmail").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Escriba correctamente el correo electrónico</div>')

            return false;

        }

        if (validarEmailRepetido) {

            $("#regEmail").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> El correo electrónico ya existe en la base de datos, por favor ingrese otro diferente</div>')

            return false;

        }

    } else {

        $("#regEmail").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Este campo es obligatorio</div>')

        return false;
    }


    /*=============================================
    VALIDAR CONTRASEÑA
    =============================================*/

    var password = $("#regPassword").val();

    if (password != "") {

        var expresion = /^[a-zA-Z0-9]*$/;

        if (!expresion.test(password)) {

            $("#regPassword").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> No se permiten caracteres especiales</div>')

            return false;

        }

    } else {

        $("#regPassword").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Este campo es obligatorio</div>')

        return false;
    }

    /*=============================================
    VALIDAR POLÍTICAS DE PRIVACIDAD
    =============================================*/

    var politicas = $("#regPoliticas:checked").val();

    if (politicas != "on") {

        $("#regPoliticas").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Debe aceptar nuestras condiciones de uso y políticas de privacidad</div>')

        return false;

    }

    return true;
}

/*=============================================
CAMBIAR FOTO
=============================================*/

$("#btnCambiarFoto").click(function() {

    $("#imgPerfil").toggle();
    $("#subirImagen").toggle();

})

$("#datosImagen").change(function() {

    var imagen = this.files[0];

    /*=============================================
    VALIDAMOS EL FORMATO DE LA IMAGEN
    =============================================*/

    if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png") {

        $("#datosImagen").val("");

        swal({
                title: "Error al subir la imagen",
                text: "¡La imagen debe estar en formato JPG o PNG!",
                type: "error",
                confirmButtonText: "¡Cerrar!",
                closeOnConfirm: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location = rutaOculta + "perfil";
                }
            });

    } else if (Number(imagen["size"]) > 2000000) {

        $("#datosImagen").val("");

        swal({
                title: "Error al subir la imagen",
                text: "¡La imagen no debe pesar más de 2 MB!",
                type: "error",
                confirmButtonText: "¡Cerrar!",
                closeOnConfirm: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location = rutaOculta + "perfil";
                }
            });

    } else {

        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);

        $(datosImagen).on("load", function(event) {

            var rutaImagen = event.target.result;
            $(".previsualizar").attr("src", rutaImagen);

        })

    }


})

/*=============================================
COMENTARIOS ID
=============================================*/

$(".calificarProducto").click(function() {

    var idComentario = $(this).attr("idComentario");

    $("#idComentario").val(idComentario);

})

/*=============================================
COMENTARIOS CAMBIO DE ESTRELLAS
=============================================*/

$("input[name='puntaje']").change(function() {

    var puntaje = $(this).val();

    switch (puntaje) {

        case "0.5":
            $("#estrellas").html('<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i>');
            break;

        case "1.0":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i>');
            break;

        case "1.5":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i>');
            break;

        case "2.0":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i>');
            break;

        case "2.5":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i>');
            break;

        case "3.0":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i>');
            break;

        case "3.5":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i>');
            break;

        case "4.0":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o text-success" aria-hidden="true"></i>');
            break;

        case "4.5":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i>');
            break;

        case "5.0":
            $("#estrellas").html('<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i> ' +
                '<i class="fa fa-star text-success" aria-hidden="true"></i>');
            break;

    }

})

/*=============================================
OBTIENE PARAMETROS DE LA URL
=============================================*/
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
/*=============================================
VALIDAR EL COMENTARIO
=============================================*/

function validarComentario() {

    var comentario = $("#comentario").val();

    if (comentario != "") {

        var expresion = /^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]*$/;

        if (!expresion.test(comentario)) {

            $("#comentario").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');

            return false;

        }

    } else {

        $("#comentario").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');

        return false;

    }

    return true;

}

/*=============================================
ELIMINAR DOMICILIO
=============================================*/

function eliminadomicilio(idDomicilio) {

    swal({
            title: "¿Está usted seguro(a) de eliminar el domicilio?",
            text: "¡Si borra este domicilio ya no se pueden recuperar los datos!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Si, borrar domicilio de entrega!",
            closeOnConfirm: false
        },
        function(isConfirm) {
            if (isConfirm) {
                //--------------------------------------
                //$(this).parent().parent().parent().remove();

                var datos = new FormData();
                datos.append("idDomicilio", idDomicilio);

                $.ajax({
                    url: rutaOculta + "ajax/usuarios.ajax.php",
                    method: "POST",
                    data: datos,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(respuesta) {
                        window.location = "index.php?ruta=perfil";
                    }

                });
                //--------------------------------------

            }
        });

}

/*=============================================
VALIDAR DOMICILIO
=============================================*/
function setdomicilio(_calle, _cp, _municipioColonia, _estado, _pais, _nota, _idDomicilio) {

    $("#domicilioCalle").val(_calle);
    $("#domicilioCp").val(_cp);
    $("#domicilioColonia").val(_municipioColonia);
    $("#domicilioEdo").val(_estado);
    $("#domicilioPais").val(_pais);
    $("#domicilioNota").val(_nota);
    $("#hdIdomicilio").val(_idDomicilio);


    if (_idDomicilio == '0') {
        $("#domicilioTitulo").text('DOMICILIO DE ENTREGA (NUEVO)');
    } else {
        $("#domicilioTitulo").text('DOMICILIO DE ENTREGA (MODIFICAR)');
    }


}

function validardomicilio() {
    var expresion = /^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]*$/;

    var domicilioCalle = $("#domicilioCalle").val();
    if (domicilioCalle != "") {
        if (!expresion.test(domicilioCalle)) {
            $("#domicilioCalle").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#domicilioCalle").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }


    var domicilioCp = $("#domicilioCp").val();
    if (domicilioCp != "") {
        if (!expresion.test(domicilioCp)) {
            $("#domicilioCp").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#domicilioCp").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }



    var domicilioColonia = $("#domicilioColonia").val();
    if (domicilioColonia != "") {
        if (!expresion.test(domicilioColonia)) {
            $("#domicilioColonia").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#domicilioColonia").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }



    var domicilioEdo = $("#domicilioEdo").val();
    if (domicilioEdo != "") {
        if (!expresion.test(domicilioEdo)) {
            $("#domicilioEdo").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#domicilioPais").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }


    var domicilioPais = $("#domicilioPais").val();
    if (domicilioPais != "") {
        if (!expresion.test(domicilioPais)) {
            $("#domicilioPais").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#domicilioPais").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }


    var domicilioNota = $("#domicilioNota").val();
    if (domicilioNota != "") {
        if (!expresion.test(domicilioNota)) {
            $("#domicilioNota").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#domicilioNota").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }

    return true;

}

/*=============================================
VALIDAR FORMAPAGO
=============================================*/
function detectaTipoTC(obj) {
    //console.log(obj.value);
    $("#pagotipo").val(GetCardType(obj.value));
}

function justNumbers(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function GetCardType(number) {
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null)
        return "Visa";

    // Mastercard 
    // Updated for Mastercard 2017 BINs expansion
    if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number))
        return "Mastercard";

    // AMEX
    re = new RegExp("^3[47]");
    if (number.match(re) != null)
        return "AMEX";

    // Discover
    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
        return "Discover";

    // Diners
    re = new RegExp("^36");
    if (number.match(re) != null)
        return "Diners";

    // Diners - Carte Blanche
    re = new RegExp("^30[0-5]");
    if (number.match(re) != null)
        return "Diners - Carte Blanche";

    // JCB
    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
        return "JCB";

    // Visa Electron
    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
    if (number.match(re) != null)
        return "Visa Electron";

    return "";
}

function setformapago(_cuenta, _nombre, _fechacaducidad, _tipo, _idformapago) {

    $("#pagotarjeta").val(_cuenta);
    $("#pagonombre").val(_nombre);
    $("#pagovigencia").val(_fechacaducidad);
    $("#pagotipo").val(_tipo);
    $("#hdIdformapago").val(_idformapago);



    if (_idformapago == '0') {
        $("#pagoTitulo").text('FORMA DE PAGO (NUEVO)');
    } else {
        $("#pagoTitulo").text('FORMA DE PAGO (MODIFICAR)');
    }


}

function validarformapago() {
    var expresion = /^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]*$/;
    var expresion2 = /^[0-9/]*$/;

    var pagotarjeta = $("#pagotarjeta").val();
    if (pagotarjeta != "") {
        if (!expresion.test(pagotarjeta)) {
            $("#pagotarjeta").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#pagotarjeta").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }


    var pagonombre = $("#pagonombre").val();
    if (pagonombre != "") {
        if (!expresion.test(pagonombre)) {
            $("#pagonombre").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#pagonombre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }


    var pagovigencia = $("#pagovigencia").val();
    if (pagovigencia != "") {
        if (!expresion2.test(pagovigencia)) {
            $("#pagovigencia").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#pagovigencia").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Campo obligatorio</div>');
        return false;
    }



    var pagotipo = $("#pagotipo").val();
    if (pagotipo != "") {
        if (!expresion.test(pagotipo)) {
            $("#pagotipo").parent().before('<div class="alert alert-danger"><strong>ERROR:</strong> No se permiten caracteres especiales como por ejemplo !$%&/?¡¿[]*</div>');
            return false;
        }
    } else {
        $("#pagotipo").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Número y tipo de tarjeta inválido. Verificque porfavor</div>');
        return false;
    }

    if ($("#pagotarjeta").val().length < 15) {
        $("#pagotarjeta").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> La longuitud debe ser 15 o 16 dígitos</div>');
        return false;
    }

    if ($("#pagonombre").val().length < 5) {
        $("#pagonombre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> El nombre es demasiado corto. Verifique por fafor</div>');
        return false;
    }

    var arrCaducidad = ($("#pagovigencia").val()).split("/");
    if (arrCaducidad.length != 2) {
        $("#pagovigencia").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> La vigencia de la tarjeta debe estar en formato MM/YY. Verifique por fafor</div>');
        return false;
    }

    if (arrCaducidad[0] > 12 || arrCaducidad[0] == 0) {
        $("#pagovigencia").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> El mes de la vigencia de la tarjeta debe estar entre 01 y 12. Verifique por fafor</div>');
        return false;
    }

    var fecha = new Date();
    var anioActual = parseInt(fecha.getFullYear().toString().substr(2, 2));
    console.log(anioActual);

    if (arrCaducidad[1] < anioActual) {
        $("#pagovigencia").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> La tarjeta ha caducado. Verifique por fafor</div>');
        return false;
    }

    if (arrCaducidad[1] > anioActual + 6) {
        $("#pagovigencia").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> La vigencia de la tarjeta tiene un valor muy grande. Verifique por fafor</div>');
        return false;
    }

    return true;

}


/*=============================================
LISTA DE DESEOS
=============================================*/

$(".deseos").click(function() {

    var idProducto = $(this).attr("idProducto");
    console.log("idProducto", idProducto);

    var idUsuario = localStorage.getItem("usuario");
    console.log("idUsuario", idUsuario);

    if (idUsuario == null) {

        swal({
                title: "Debe ingresar al sistema",
                text: "¡Para agregar un producto a la 'lista de deseos' debe primero ingresar al sistema!",
                type: "warning",
                confirmButtonText: "¡Cerrar!",
                closeOnConfirm: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location = rutaOculta;
                }
            });

    } else {

        $(this).addClass("btn-danger");

        var datos = new FormData();
        datos.append("idUsuario", idUsuario);
        datos.append("idProducto", idProducto);

        $.ajax({
            url: rutaOculta + "ajax/usuarios.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {


            }

        })

    }

})

/*=============================================
BORRAR PRODUCTO DE LISTA DE DESEOS
=============================================*/

$(".quitarDeseo").click(function() {

    var idDeseo = $(this).attr("idDeseo");

    $(this).parent().parent().parent().remove();

    var datos = new FormData();
    datos.append("idDeseo", idDeseo);

    $.ajax({
        url: rutaOculta + "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {

        }

    });


})

/*=============================================
ELIMINAR USUARIO
=============================================*/

$("#eliminarUsuario").click(function() {

    var id = $("#idUsuario").val();

    if ($("#modoUsuario").val() == "directo") {

        if ($("#fotoUsuario").val() != "") {

            var foto = $("#fotoUsuario").val();

        }

    }

    swal({
            title: "¿Está usted seguro(a) de eliminar su cuenta?",
            text: "¡Si borrar esta cuenta ya no se puede recuperar los datos!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Si, borrar cuenta!",
            closeOnConfirm: false
        },
        function(isConfirm) {
            if (isConfirm) {
                window.location = "index.php?ruta=perfil&id=" + id + "&foto=" + foto;
            }
        });

})


/*=============================================
VALIDACIÓN FORMULARIO CONTACTENOS
=============================================*/

function validarContactenos() {

    var nombre = $("#nombreContactenos").val();
    var email = $("#emailContactenos").val();
    var mensaje = $("#mensajeContactenos").val();

    /*=============================================
    VALIDACIÓN DEL NOMBRE
    =============================================*/

    if (nombre == "") {

        $("#nombreContactenos").before('<h6 class="alert alert-danger">Escriba por favor el nombre</h6>');

        return false;

    } else {

        var expresion = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/;

        if (!expresion.test(nombre)) {

            $("#nombreContactenos").before('<h6 class="alert alert-danger">Escriba por favor sólo letras sin caracteres especiales</h6>');

            return false;

        }

    }

    /*=============================================
    VALIDACIÓN DEL EMAIL
    =============================================*/

    if (email == "") {

        $("#emailContactenos").before('<h6 class="alert alert-danger">Escriba por favor el email</h6>');

        return false;

    } else {

        var expresion = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

        if (!expresion.test(email)) {

            $("#emailContactenos").before('<h6 class="alert alert-danger">Escriba por favor correctamente el correo electrónico</h6>');

            return false;
        }

    }

    /*=============================================
    VALIDACIÓN DEL MENSAJE
    =============================================*/

    if (mensaje == "") {

        $("#mensajeContactenos").before('<h6 class="alert alert-danger">Escriba por favor un mensaje</h6>');

        return false;

    } else {

        var expresion = /^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]*$/;

        if (!expresion.test(mensaje)) {

            $("#mensajeContactenos").before('<h6 class="alert alert-danger">Escriba el mensaje sin caracteres especiales</h6>');

            return false;
        }

    }

    return true;
}