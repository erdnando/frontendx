<!--=====================================
VALIDAR SESIÓN
======================================-->

<?php

$url = Ruta::ctrRuta();
$servidor = Ruta::ctrRutaServidor();

if(!isset($_SESSION["validarSesion"])){

	echo '<script>
	
		window.location = "'.$url.'";

	</script>';

	exit();

}

?>

<!--=====================================
BREADCRUMB PERFIL
======================================-->

<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
SECCIÓN PERFIL
======================================-->

<div class="container-fluid">

	<div class="container" id="tabsPerfil">

		<ul class="nav nav-tabs">
		  
	  		<li >	  			
			  	<a data-toggle="tab" href="#compras" id="tabCompras">
			  	<i class="fa fa-list-ul"></i> MIS COMPRAS</a>
	  		</li>

	  		<li> 				
		  		<a data-toggle="tab" href="#deseos" id="tabDeseos">
		  		<i class="fa fa-gift"></i> MI LISTA DE DESEOS</a>
	  		</li>

	  		<li class="active" > 		 <!--class="active"	-->	
	  			<a data-toggle="tab" href="#perfil" id="tabPerfil">
	  			<i class="fa fa-user"></i> EDITAR PERFIL</a>
	  		</li>

			<li >				
	  			<a data-toggle="tab" href="#direcciones" id="tabDirecciones">
	  			<i class="fa fa-home"></i> MIS DIRECCIONES</a>
	  		</li>

			<li>				
	  			<a data-toggle="tab" href="#formasPago" id="tabFormasPago">
	  			<i class="fa fa-user"></i> MIS FORMAS DE PAGO</a>
	  		</li>


	  		<li>				
		 	 	<a href="<?php echo $url; ?>ofertas" id="tabOfertas">
		 	 	<i class="fa fa-star"></i>	VER OFERTAS</a>
	  		</li>
		
		</ul>

		<div class="tab-content">

			<!--=====================================
			PESTAÑA COMPRAS
			======================================-->

	  		<div id="compras" class="tab-pane fade">
		    
				<div class="panel-group">

				<?php

					$item = "id_usuario";
					$valor = $_SESSION["id"];

					$compras = ControladorUsuarios::ctrMostrarCompras($item, $valor);

					if(!$compras){

						echo '<div class="col-xs-12 text-center error404">
				               
				    		<h1><small>¡Oops!</small></h1>
				    
				    		<h2>Aún no tienes compras realizadas en esta tienda</h2>

				  		</div>';

					}else{

						foreach ($compras as $key => $value1) {

							$ordenar = "id";
							$item = "id";
							$valor = $value1["id_producto"];

							$productos = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

							foreach ($productos as $key => $value2) {
							
								echo '<div class="panel panel-default">
									    
									    <div class="panel-body">

											<div class="col-md-2 col-sm-6 col-xs-12">

												<figure>
												
													<img class="img-thumbnail" src="'.$servidor.$value2["portada"].'">

												</figure>

											</div>

											<div class="col-sm-6 col-xs-12">

												<h1><small>'.$value2["titulo"].'</small></h1>

												<p>'.$value2["titular"].'</p>';

												if($value2["tipo"] == "virtual"){

													echo '<a href="'.$url.'curso/'.$value1["id"].'/'.$value1["id_usuario"].'/'.$value1["id_producto"].'/'.$value2["ruta"].'">
														<button class="btn btn-default pull-left">Ir al curso</button>
														</a>';

												}else{

													echo '<h6>Proceso de entrega: '.$value2["entrega"].' días hábiles</h6>';

													if($value1["envio"] == 0){

														echo '<div class="progress">

															<div class="progress-bar progress-bar-info" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Despachado
															</div>

															<div class="progress-bar progress-bar-default" role="progressbar" style="width:33.33%">
																<i class="fa fa-clock-o"></i> Enviando
															</div>

															<div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
																<i class="fa fa-clock-o"></i> Entregado
															</div>

														</div>';

													}

													if($value1["envio"] == 1){

														echo '<div class="progress">

															<div class="progress-bar progress-bar-info" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Despachado
															</div>

															<div class="progress-bar progress-bar-default" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Enviando
															</div>

															<div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
																<i class="fa fa-clock-o"></i> Entregado
															</div>

														</div>';

													}

													if($value1["envio"] == 2){

														echo '<div class="progress">

															<div class="progress-bar progress-bar-info" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Despachado
															</div>

															<div class="progress-bar progress-bar-default" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Enviando
															</div>

															<div class="progress-bar progress-bar-success" role="progressbar" style="width:33.33%">
																<i class="fa fa-check"></i> Entregado
															</div>

														</div>';

													}

												}

												echo '<h4 class="pull-right"><small>Comprado el '.substr($value1["fecha"],0,-8).'</small></h4>
																
											</div>

											<div class="col-md-4 col-xs-12">';

											$datos = array("idUsuario"=>$_SESSION["id"],
															"idProducto"=>$value2["id"] );

											$comentarios = ControladorUsuarios::ctrMostrarComentariosPerfil($datos);

												echo '<div class="pull-right">

													<a class="calificarProducto" href="#modalComentarios" data-toggle="modal" idComentario="'.$comentarios["id"].'">
													
														<button class="btn btn-default backColor">Calificar Producto</button>

													</a>

												</div>

												<br><br>

												<div class="pull-right">

													<h3 class="text-right">';

													if($comentarios["calificacion"] == 0 && $comentarios["comentario"] == ""){

														echo '<i class="fa fa-star-o text-success" aria-hidden="true"></i>
																<i class="fa fa-star-o text-success" aria-hidden="true"></i>
																<i class="fa fa-star-o text-success" aria-hidden="true"></i>
																<i class="fa fa-star-o text-success" aria-hidden="true"></i>
																<i class="fa fa-star-o text-success" aria-hidden="true"></i>';

													}else{

														switch($comentarios["calificacion"]){

															case 0.5:
															echo '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 1.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 1.5:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 2.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 2.5:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 3.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 3.5:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 4.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
															break;

															case 4.5:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>';
															break;

															case 5.0:
															echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>
																  <i class="fa fa-star text-success" aria-hidden="true"></i>';
															break;

														}


													}
												
														
													echo '</h3>

													<p class="panel panel-default text-right" style="padding:5px">

														<small>

														'.$comentarios["comentario"].'

														</small>
													
													</p>

												</div>

											</div>

									    </div>

									</div>';

							}
							
						}
					}
				?>
				  
				

				</div>

		  	</div>

		  	<!--=====================================
			PESTAÑA DESEOS
			======================================-->
		  	<div id="deseos" class="tab-pane fade">
		    	
			<?php

				$item = $_SESSION["id"];

				$deseos = ControladorUsuarios::ctrMostrarDeseos($item);

				if(!$deseos){

					echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center error404">
				               
			    		<h1><small>¡Oops!</small></h1>
			    
			    		<h2>Aún no tiene productos en su lista de deseos</h2>

			  		</div>';
				
				}else{

					foreach ($deseos as $key => $value1) {

						$ordenar = "id";
						$valor = $value1["id_producto"];
						$item = "id";

						$productos = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

						echo '<ul class="grid0">';

							foreach ($productos as $key => $value2) {
							
							echo '<li class="col-md-3 col-sm-6 col-xs-12">

									<figure>
										
										<a href="'.$url.$value2["ruta"].'" class="pixelProducto">
											
											<img src="'.$servidor.$value2["portada"].'" class="img-responsive">

										</a>

									</figure>

									<h4>
							
										<small>
											
											<a href="'.$url.$value2["ruta"].'" class="pixelProducto">
												
												'.$value2["titulo"].'<br>

												<span style="color:rgba(0,0,0,0)">-</span>';

												$fecha = date('Y-m-d');
												$fechaActual = strtotime('-30 day', strtotime($fecha));
												$fechaNueva = date('Y-m-d', $fechaActual);

												if($fechaNueva < $value["fecha"]){

													echo '<span class="label label-warning fontSize">Nuevo</span> ';

												}

												if($value2["oferta"] != 0){

													echo '<span class="label label-warning fontSize">'.$value2["descuentoOferta"].'% off</span>';

												}

											echo '</a>	

										</small>			

									</h4>

									<div class="col-xs-6 precio">';

									if($value2["precio"] == 0){

										echo '<h2 style="margin-top:-10px"><small>GRATIS</small></h2>';

									}else{

										if($value2["oferta"] != 0){

											echo '<h2 style="margin-top:-10px">

													<small>
								
														<strong class="oferta" style="font-size:12px">MXN $'.$value2["precio"].'</strong>

													</small>

													<small>$'.$value2["precioOferta"].'</small>
												
												</h2>';

										}else{

											echo '<h2 style="margin-top:-10px"><small>MXN $'.$value2["precio"].'</small></h2>';

										}
										
									}
													
									echo '</div>

									<div class="col-xs-6 enlaces">
										
										<div class="btn-group pull-right">
											
											<button type="button" class="btn btn-danger btn-xs quitarDeseo" idDeseo="'.$value1["id"].'" data-toggle="tooltip" title="Quitar de mi lista de deseos">
												
												<i class="fa fa-heart" aria-hidden="true"></i>

											</button>';

											if($value2["tipo"] == "virtual" && $value2["precio"] != 0){

												if($value2["oferta"] != 0){

													echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value2["id"].'" imagen="'.$servidor.$value2["portada"].'" titulo="'.$value2["titulo"].'" precio="'.$value2["precioOferta"].'" tipo="'.$value2["tipo"].'" peso="'.$value2["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

													<i class="fa fa-shopping-cart" aria-hidden="true"></i>

													</button>';

												}else{

													echo '<button type="button" class="btn btn-default btn-xs agregarCarrito"  idProducto="'.$value2["id"].'" imagen="'.$servidor.$value2["portada"].'" titulo="'.$value2["titulo"].'" precio="'.$value2["precio"].'" tipo="'.$value2["tipo"].'" peso="'.$value2["peso"].'" data-toggle="tooltip" title="Agregar al carrito de compras">

													<i class="fa fa-shopping-cart" aria-hidden="true"></i>

													</button>';

												}

											}

											echo '<a href="'.$url.$value2["ruta"].'" class="pixelProducto">
											
												<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver producto">
													
													<i class="fa fa-eye" aria-hidden="true"></i>

												</button>	
											
											</a>

										</div>

									</div>

								</li>';
							}

						echo '</ul>';


					}

				}

			?>
		  	</div>
			<!--=====================================
			PESTAÑA PERFIL
			======================================-->
			<!--in active-->
		  	<div id="perfil" class="tab-pane fade in active">
		    	
				<div class="row">
					
					<form method="post" enctype="multipart/form-data">
					
						<div class="col-md-3 col-sm-4 col-xs-12 text-center">
							
							<br>

							<figure id="imgPerfil">
								
							<?php

							echo '<input type="hidden" value="'.$_SESSION["id"].'" id="idUsuario" name="idUsuario">
							      <input type="hidden" value="'.$_SESSION["password"].'" name="passUsuario">
							      <input type="hidden" value="'.$_SESSION["foto"].'" name="fotoUsuario" id="fotoUsuario">
							      <input type="hidden" value="'.$_SESSION["modo"].'" name="modoUsuario" id="modoUsuario">';


							if($_SESSION["modo"] == "directo"){

								if($_SESSION["foto"] != ""){

									echo '<img src="'.$url.$_SESSION["foto"].'" class="img-thumbnail">';

								}else{

									echo '<img src="'.$servidor.'vistas/img/usuarios/default/anonymous.png" class="img-thumbnail">';

								}
					

							}else{

								echo '<img src="'.$_SESSION["foto"].'" class="img-thumbnail">';
							}		

							?>

							</figure>

							<br>

							<!-- <?php

							if($_SESSION["modo"] == "directo"){
							echo '<button type="button" class="btn btn-default" id="btnCambiarFoto">
									Cambiar foto de perfil
									</button>';
							}

							?> -->

							<div id="subirImagen">
								
								<input type="file" class="form-control" id="datosImagen" name="datosImagen">

								<img class="previsualizar">

							</div>

						</div>	

						<div class="col-md-9 col-sm-8 col-xs-12">

						<br>
							
						<?php

						if($_SESSION["modo"] != "directo"){

							echo '<label class="control-label text-muted text-uppercase">Nombre:</label>
									
									<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										<input type="text" class="form-control"  value="'.$_SESSION["nombre"].'" readonly>

									</div>

									<br>

									<label class="control-label text-muted text-uppercase">Correo electrónico:</label>
									
									<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										<input type="text" class="form-control"  value="'.$_SESSION["email"].'" readonly>

									</div>

									<br>

									<label class="control-label text-muted text-uppercase">Modo de registro en el sistema:</label>
									
									<div class="input-group">
								
										<span class="input-group-addon"><i class="fa fa-'.$_SESSION["modo"].'"></i></span>
										<input type="text" class="form-control text-uppercase"  value="'.$_SESSION["modo"].'" readonly>

									</div>

									<br>';
		

						}else{

							echo '<label class="control-label text-muted text-uppercase" for="editarNombre">Cambiar Nombre:</label>
									
									<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										<input type="text" class="form-control" id="editarNombre" name="editarNombre" value="'.$_SESSION["nombre"].'">

									</div>

								<br>

								<label class="control-label text-muted text-uppercase" for="editarEmail">Cambiar Correo Electrónico:</label>

								<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
										<input type="text" class="form-control" id="editarEmail" name="editarEmail" value="'.$_SESSION["email"].'">

									</div>

								<br>

								<label class="control-label text-muted text-uppercase" for="editarPassword">Cambiar Contraseña:</label>

								<div class="input-group">
								
										<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
										<input type="text" class="form-control" id="editarPassword" name="editarPassword" placeholder="Escribe la nueva contraseña">

									</div>

								<br>

								<button type="submit" class="btn btn-default backColor btn-md pull-left">Actualizar Datos</button>';

						}

						?>

						</div>

						<?php

							$actualizarPerfil = new ControladorUsuarios();
							$actualizarPerfil->ctrActualizarPerfil();

						?>					

					</form>

					<button class="btn btn-danger btn-md pull-right" id="eliminarUsuario">Eliminar cuenta</button>
					<?php
							$borrarUsuario = new ControladorUsuarios();
							$borrarUsuario->ctrEliminarUsuario();
						?>	
				</div>

		  	</div>
            <!--=====================================
			  MIS DIRECCIONES
			======================================-->
	  		<div id="direcciones" class="tab-pane fade">
		    
				<div class="panel-group">

				<?php

					$item = "id_usuario";
					$valor = $_SESSION["id"];

					$direcciones = ControladorUsuarios::ctrMostrarDirecciones($item, $valor);

					if(!$direcciones){
						echo '<div class="col-xs-12 text-center error404">
				               
				    		<h1><small>¡Oops!</small></h1>
				    
							<h2>Aún no tienes direcciones registradas para el envío en esta tienda</h2>
							<a class="calificarProducto" href="#modalDomicilios" onclick="setdomicilio( \'\'  ,  \'\'  ,  \'\'   ,   \'\'   ,   \'\'    ,   \'\'    ,   \'0\'   )" data-toggle="modal" idComentario=0>
								<button class="btn btn-success">Ingresa una dirección de entrega</button>
							</a>

				  		</div>';
					}else{
						foreach ($direcciones as $key => $direccion) {	
								echo '<div class="panel panel-default">
								
									    <div class="panel-body">
											<div class="col-md-2 col-sm-6 col-xs-12">
												<figure style="text-align: center;">
												<img style="width:70%" class="img-thumbnail" src="'.$servidor.'vistas/img/iconos/domicilio.png">
												</figure>
											</div>

											<div class="col-sm-6 col-xs-12">
												<h1><small>'.$direccion["calle"].'</small></h1>
												<p></p>';
												echo '<h6>'.$direccion["cp"].', '.$direccion["municipio-colonia"].'</h6>';
												echo '<h6>'.$direccion["estado"].', '.$direccion["pais"].'</h6>';
												echo '<h6>'.$direccion["nota"].'</h6>
											</div>

											<div class="col-md-4 col-xs-12">
												<div class="pull-right">
												<a class="calificarProducto"  href="#modalDomicilios" onclick="setdomicilio(\''.$direccion["calle"] .'\' ,  \''.$direccion["cp"] .'\'  ,  \''.$direccion["municipio-colonia"] .'\'   ,   \''.$direccion["estado"] .'\'   ,   \''.$direccion["pais"] .'\'    ,   \''.$direccion["nota"] .'\'    ,   \''.$direccion["id"] .'\'   )" data-toggle="modal" idDireccion='.$direccion["id"].'>
													<button class="btn btn-success" >Editar</button>
												</a>

												<button id="eliminarDomicilio" onclick="eliminadomicilio( \''.$direccion["id"] .'\')" class="btn btn-danger">Eliminar</button>
												
												<a class="calificarProducto" href="#modalDomicilios" onclick="setdomicilio( \'\'  ,  \'\'  ,  \'\'   ,   \'\'   ,   \'\'    ,   \'\'    ,   \'0\'   )" data-toggle="modal" idComentario=0>
													<button class="btn btn-default">Nueva</button>
												</a>
												</div>
											</div>

										</div>

									  </div>';	
						}
					}
				?>
				
				  
				</div>

		  	</div>
			<!--=====================================
			  MIS FORMAS DE PAGO
			======================================-->
			<div id="formasPago" class="tab-pane fade">
		    
				<div class="panel-group">

				<?php

					$item = "id_usuario";
					$valor = $_SESSION["id"];

					$formasPago = ControladorUsuarios::ctrMostrarformasPago($item, $valor);

					if(!$formasPago){
						echo '<div class="col-xs-12 text-center error404">
							
							<h1><small>¡Oops!</small></h1>
					
							<h2>Aún no tienes formas de pago registradas esta tienda</h2>
							<a class="calificarProducto" href="#modalFormaPago" onclick="setformapago( \'\'  ,  \'\'  ,  \'\'   ,  \'\'   ,   \'0\'   )" data-toggle="modal" idComentario=0>
								<button class="btn btn-success">Ingresa una forma de pago</button>
							</a>

						</div>';
					}else{
						foreach ($formasPago as $key => $pago) {

							$cuentaFormateada = "**** **** ****". substr($pago["cuenta"], strlen($pago["cuenta"])-4 , 4) ;
							
							$nombreMayuscula= strtoupper($pago["nombre"]);
							
								echo '<div class="panel panel-default">
								
										<div class="panel-body">
											<div class="col-md-2 col-sm-6 col-xs-12">
												<figure style="text-align: center;">
												<img  class="img-thumbnail" src="'.$servidor.'vistas/img/iconos/formapago1.jpg">
												</figure>
											</div>

											<div class="col-sm-6 col-xs-12">
												<h1 style=\'margin-top:-5px;!important\'><small>'.$cuentaFormateada.'</small></h1>
												<p></p>';
												echo '<h6>'.$nombreMayuscula.'</h6>';
												echo '<h6>'.$pago["fechacaducidad"].'</h6>';
												echo '<h6>'.$pago["tipo"].'</h6>
											</div>

											<div class="col-md-4 col-xs-12">
												<div class="pull-right">
												<a class="calificarProducto"  href="#modalFormaPago" onclick="setformapago(\''.$pago["cuenta"] .'\' ,\''.$pago["nombre"] .'\' ,  \''.$pago["fechacaducidad"] .'\'  ,  \''.$pago["tipo"] .'\' ,  \''.$pago["id"] .'\'  )" data-toggle="modal" idPago='.$pago["id"].'>
													<button class="btn btn-success" >Editar</button>
												</a>
												<button id="eliminarDomicilio" onclick="eliminaformapago( \''.$pago["id"] .'\')" class="btn btn-danger">Eliminar</button>
												<a class="calificarProducto" href="#modalFormaPago" onclick="setformapago( \'\'  ,  \'\'  ,  \'\'  , \'\'  ,  \'0\'   )" data-toggle="modal" idPago=0>
													<button class="btn btn-default">Nueva</button>
												</a>
												</div>
											</div>

										</div>
									</div>';	
						}
					}
				?> 
				</div>

		    </div>
			  <!--=====================================
			  FIN TABS
			======================================-->


		</div>

	</div>

</div>

<!--=====================================
VENTANA MODAL PARA COMENTARIOS
======================================-->

<div  class="modal fade modalFormulario" id="modalComentarios" role="dialog">
	
	<div class="modal-content modal-dialog">
		
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor">CALIFICA ESTE PRODUCTO</h3>

			<button type="button" class="close" data-dismiss="modal">&times;</button>

			<form method="post" onsubmit="return validarComentario()">

				<input type="hidden" value="" id="idComentario" name="idComentario">
				
				<h1 class="text-center" id="estrellas">

		       		<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>

				</h1>

				<div class="form-group text-center">

		       		<label class="radio-inline"><input type="radio" name="puntaje" value="0.5">0.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="1.0">1.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="1.5">1.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="2.0">2.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="2.5">2.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="3.0">3.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="3.5">3.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="4.0">4.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="4.5">4.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="5.0" checked>5.0</label>

				</div>

				<div class="form-group">
			  		
			  		<label for="comment" class="text-muted">Tu opinión acerca de este producto: <span><small>(máximo 300 caracteres)</small></span></label>
			  		
			  		<textarea class="form-control" rows="5" id="comentario" name="comentario" maxlength="300" required></textarea>

			  		<br>
					
					<input type="submit" class="btn btn-default backColor btn-block" value="ENVIAR">

				</div>

				<?php

					$actualizarComentario = new ControladorUsuarios();
					$actualizarComentario -> ctrActualizarComentario();

				?>

			</form>

		</div>

		<div class="modal-footer">
      	
      	</div>

	</div>

</div>


<!--=====================================
VENTANA MODAL PARA CRUD DE DOMICILIOS
======================================-->

<div  class="modal fade modalFormulario" id="modalDomicilios" role="dialog">
	
	<div class="modal-content modal-dialog">
		
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor"> <span id=domicilioTitulo></span> </h3>

			<button  type="button" class="close" data-dismiss="modal">&times;</button>

			<form method="post" onsubmit="return validardomicilio(this)">

				<div class="col-md-12 col-sm-12 col-xs-12">
						<?php
							echo '
									<input type="hidden" value="" id="hdIdomicilio" name="hdIdomicilio">
									<label class="control-label text-muted text-uppercase" for="domicilioCalle">Calle y colonia:</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
										<input type="text" class="form-control" id="domicilioCalle" name="domicilioCalle" value="'.$_SESSION["nombre"].'" placeholder="Escribe los datos de calle y colonia">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="domicilioCp">Código postal:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
										<input type="text" class="form-control" id="domicilioCp" name="domicilioCp" value="'.$_SESSION["email"].'" placeholder="Escribe los datos de CP">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="domicilioColonia">Ciudad/municipio:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
										<input type="text" class="form-control" id="domicilioColonia" name="domicilioColonia" value="'.$_SESSION["email"].'" placeholder="Escribe los datos de Colonia o municipio">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="domicilioEdo">Estado:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
										<input type="text" class="form-control" id="domicilioEdo" name="domicilioEdo"  placeholder="Escribe los datos de Estado">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="domicilioPais">Pais:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
										<input type="text" class="form-control" id="domicilioPais" name="domicilioPais"  placeholder="Escribe los datos del pais">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="domicilioNota">Descripción del domicilio:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
										<input type="text" class="form-control" id="domicilioNota" name="domicilioNota" placeholder="Escribe alguna descripción del domicilio">
									</div>
								<br>

								<button type="submit" class="btn btn-default backColor btn-md pull-right">Aceptar</button>';
						?>
						</div>
				<?php
					$actualizarDomicilio = new ControladorUsuarios();
					$actualizarDomicilio -> ctrActualizardomicilio();
				?>
			</form>

		</div>

		<div class="modal-footer">
      	
      	</div>

	</div>

</div>



<!--=====================================
VENTANA MODAL PARA CRUD DE FORMAS DE PAGO
======================================-->

<div  class="modal fade modalFormulario" id="modalFormaPago" role="dialog">
	
	<div class="modal-content modal-dialog">
		
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor"> <span id=pagoTitulo></span> </h3>

			<button  type="button" class="close" data-dismiss="modal">&times;</button>

			<form method="post" onsubmit="return validarformapago(this)">

				<div class="col-md-12 col-sm-12 col-xs-12">
						<?php
							echo '
									<input type="hidden" value="" id="hdIdformapago" name="hdIdformapago">
									<label class="control-label text-muted text-uppercase" for="pagotarjeta">No. Tarjeta:</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
										<input type="text" class="form-control" onkeypress="justNumbers(event)" onblur="detectaTipoTC(this)"  id="pagotarjeta" name="pagotarjeta" maxlength="16"  placeholder="Escriba su no. de tarjeta">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="pagonombre">Nombre:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
										<input type="text" style="text-transform: uppercase;" class="form-control" id="pagonombre" name="pagonombre" maxlength="80" placeholder="Escriba su nombre como en la tarjeta">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="pagovigencia">Vigencia (mm/yy):</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
										<input type="text" class="form-control" id="pagovigencia" name="pagovigencia" maxlength="5" placeholder="Escriba la vigencia de la tarjeta, ejemplo 01/23">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="pagotipo">tipo tarjeta:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
										<input type="text" style="text-transform: uppercase;" class="form-control" readonly id="pagotipo" name="pagotipo" maxlength="30" placeholder="Tipo tarjeta">
									</div>
								<br>

								

								<button type="submit" class="btn btn-default backColor btn-md pull-right">Aceptar</button>';
						?>
						</div>
				<?php
					$actualizarPago = new ControladorUsuarios();
					$actualizarPago -> ctrActualizarformapago();
				?>
			</form>

		</div>

		<div class="modal-footer">
      	
      	</div>

	</div>

</div>
