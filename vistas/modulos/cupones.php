<?php

$servidor = Ruta::ctrRutaServidor();
$url = Ruta::ctrRuta();

?>


<!--=====================================
CUPONES
======================================-->
<div class="container-fluid well well-sm">
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12" style="text-align:center">
					<h1><small>CUPONES DE DESCUENTO</small></h1>
			</div>
		</div>

	</div>

</div>

<!--=====================================
JUMBOTRON AVISO CUPONES
======================================-->


<div class="container-fluid">

	<div class="container">

		<div class="row" id="moduloOfertas">
			
			<?php

			$item = null;
			$valor = null;

			date_default_timezone_set('America/Mexico_City');

			$fecha = date('Y-m-d');
			$hora = date('H:i:s');
			
			$fechaActual = $fecha.' '.$hora;
			
			

			/*=============================================
			TRAEMOS LAS OFERTAS DE CUPONES
			=============================================*/

			$respuesta = ControladorProductos::ctrMostrarCategorias($item, $valor);
			//$respuesta = ControladorCarrito::ctrObtenerCupon(null);
			if($respuesta==null)

			foreach ($respuesta as $key => $value) {

				//if($value["oferta"] == 1){

					if($value["vigencia"] > $fecha){

						$datetime1 = new DateTime($value["vigencia"]);
						$datetime2 = new DateTime($fechaActual);

						$interval = date_diff($datetime1, $datetime2);

						$finOferta = $interval->format('%a');

						echo '<div class="col-md-4 col-sm-6 col-xs-12">

							<div class="ofertas">
								
								<h3 class="text-center text-uppercase">

									¡OFERTA ESPECIAL EN <br>'.$value["descripcion"].'!
								
								</h3>

								<figure>

									<img class="img-responsive" src="'.$servidor.$value["promo"].'" width="100%">

									<div class="sombraSuperior"></div>';

									if($value["tipo"] == 2){


										echo '<h1 class="text-center text-uppercase">%'.$value["descuento"].' OFF</h1>';
									
									}else{

										echo '<h1 class="text-center text-uppercase">$ '.$value["descuento"].'</h1>';

									}
			

								echo '</figure>';

								if($finOferta == 0){

									echo '<h4 class="text-center">El cupón caduca hoy</h4>';

								}else if($finOferta == 1){

									echo '<h4 class="text-center">El cupón termina en '.$finOferta.' día</h4>';

								}else{

									echo '<h4 class="text-center">El cupón termina en '.$finOferta.' días</h4>';
								}

							echo '<center>
								
								<div class="countdown" finOferta="'.$value["vigencia"].'"></div>

								

							</center>
								
							</div>

						</div>';

					}

				//}
				
			}

		

			?>


		</div>

	</div>

</div>

