<?php

    $url = Ruta::ctrRuta();
 ?>
<!--=====================================
BREADCRUMB CARRITO DE COMPRAS
======================================-->
<div class="container-fluid well well-sm">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				<li><a href="<?php echo $url;  ?>">CARRITO DE COMPRAS</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>
			</ul>
		</div>
	</div>
</div>
<!--=====================================
TABLA CARRITO DE COMPRAS
======================================-->
<div class="container-fluid">
	<div class="container">
		<div class="panel panel-default">
			<!--=====================================
			CABECERA CARRITO DE COMPRAS
			======================================-->
			<div class="panel-heading cabeceraCarrito">
				<div class="col-md-6 col-sm-7 col-xs-12 text-center">
					<h3>
						<small>PRODUCTO</small>
					</h3>
				</div>
				<div class="col-md-2 col-sm-1 col-xs-0 text-center">
					<h3>
						<small>PRECIO</small>
					</h3>
				</div>
				<div class="col-sm-2 col-xs-0 text-center">
					<h3>
						<small>CANTIDAD</small>
					</h3>
				</div>
				<div class="col-sm-2 col-xs-0 text-center">
					<h3>
						<small>SUBTOTAL</small>
					</h3>
				</div>
			</div>
			<!--=====================================
			CUERPO CARRITO DE COMPRAS
			======================================-->
			<div class="panel-body cuerpoCarrito">
			</div>
			<!--=====================================
			SUMA DEL TOTAL DE PRODUCTOS
			======================================-->
			<div class="panel-body sumaCarrito" style="padding:0px">
				<div class="col-md-4 col-sm-6 col-xs-12 pull-right well" style="padding:0px">
					<div class="col-xs-6">
						<h4>TOTAL:</h4>
					</div>
					<div class="col-xs-6">
						<h4 class="sumaSubTotal">
						</h4>
					</div> 
				</div>
			</div>
			<!--=====================================
			BOTÓN CHECKOUT
			======================================-->
			<div class="panel-heading cabeceraCheckout">
			<?php
				if(isset($_SESSION["validarSesion"])){
					if($_SESSION["validarSesion"] == "ok"){
						echo '<a id="btnCheckout" href="#modalCheckout" data-toggle="modal" idUsuario="'.$_SESSION["id"].'"><button class="btn btn-default backColor btn-lg pull-right">REALIZAR PAGO</button></a>';
					}
				}else{
					echo '<a href="#modalIngreso" data-toggle="modal"><button class="btn btn-default backColor btn-lg pull-right">REALIZAR PAGO</button></a>';
				}
			?>	
			</div>
		</div>
	</div>
</div>

<!--=====================================
VENTANA MODAL PARA CHECKOUT
======================================-->

<div id="modalCheckout" class="modal fade modalFormulario" role="dialog">
	
	 <div class="modal-content modal-dialog">
	 	
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor">REALIZAR PAGO</h3>

			<button type="button" class="close" data-dismiss="modal">&times;</button>

			<div class="contenidoCheckout">
				<?php

				$respuesta = ControladorCarrito::ctrMostrarTarifas();

				echo '<input type="hidden" id="tasaImpuesto" value="'.$respuesta["impuesto"].'">
					  <input type="hidden" id="envioNacional" value="'.$respuesta["envioNacional"].'">
				      <input type="hidden" id="envioInternacional" value="'.$respuesta["envioInternacional"].'">
				      <input type="hidden" id="tasaMinimaNal" value="'.$respuesta["tasaMinimaNal"].'">
					  <input type="hidden" id="tasaMinimaInt" value="'.$respuesta["tasaMinimaInt"].'">
					  <input type="hidden" id="hdnDescuento">
				      <input type="hidden" id="tasaPais" value="'.$respuesta["pais"].'">
				';
				?>
                <div class="container" style="width: 640px;">
					<div class="row">
					  <div class="col-md-6 col-sm-6 col-xs-12" style="margin-left: -35px;">
							<!-- direcciones de entrega list -->
							<div class="formEnvio row" style="margin-top:-10px">
								<h4 style="padding:5px;background-color: lavender;border-radius: 4px;border: 1px solid #e3e3e3;" class="text-center   text-uppercase">1.- Dirección de entrega</h4>
								
								<select class="selectpicker show-menu-arrow" id="cboDirecciones" data-style="btn-success"> 
								<option value="0" data-content="Seleccionar la dirección de entrega"></option>
								</select>
								<!--https://developer.snapappointments.com/bootstrap-select/examples/-->
							</div>
					 </div>
						<!-- formas de pago list-->
					 <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: -11px;margin-left: -13px">
					        <div class="formaPago row" >
						   
								<h4 style="padding:5px;background-color: lavender;border-radius: 4px;border: 1px solid #e3e3e3;" class="text-center   text-uppercase">2.- Elige la forma de pago</h4>
								
								<select class="selectpicker show-menu-arrow" id="cboFormaspago" data-style="btn-success"> 
									<option value="0" data-content="Seleccionar la forma de pago"></option>
								</select>	
							</div>			
					  </div>
					</div>
				</div>
                
				<!-- CVC seccion -->
				<div class="formaPagoCVC row" id="seccionCVC" style="display:none;">
					<h4 style="padding:5px;background-color: lavender;border-radius: 4px;border: 1px solid #e3e3e3;" class="text-center   text-uppercase">3.- Ingrese su CVC</h4>
					  <input type="password" id="pagoCVC" onblur="validarformapago()" onkeyup="justNumbersCVC(event)" name="pagoCVC" value="" maxlength=4
                             style="font-size: large;text-align: center;width:100%;background:cornsilk;color: black;font-weight: bolder;padding-left: 8px;" autofocus placeholder="Su CVC">			
				</div>

				<!-- Cupón de descuento -->
				<div class="formaPagoCupon row" id="seccionCupon" style="display:none;">
					<h4 style="padding:5px;background-color: lavender;border-radius: 4px;border: 1px solid #e3e3e3;" class="text-center   text-uppercase">¡Si tiene un cupón ingreselo!</h4>
					
					<div class="col-md-9 col-sm-9 col-xs-12">
					  <input class="text-uppercase" type="text" id="pagocupon"  name="pagocupon" value="" maxlength=10  onkeyup="justAlfaNumericoCupon(event)"
                             style="font-size: large;text-align: center;width:100%;background:cornsilk;color: black;font-weight: bolder;margin-left:-13px;" autofocus placeholder="Cupón">
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
					   <button class="btn btn-block btn-danger btnAplicarCupon" style="margin-left:15px;margin-top:-3px;">Aplicar</button>
					</div>
								
					
				</div>

                <!-- listado de productos por adquirir -->
				<div class="listaProductos row">
					<h4 style="padding:5px;background-color: lavender;border-radius: 4px;border: 1px solid #e3e3e3;" class="text-center  text-uppercase">Productos a comprar</h4>
					<table class="table table-striped tablaProductos">
						 <thead>
							<tr>		
								<th>Producto</th>
								<th>Cantidad</th>
								<th>Precio</th>
							</tr>
						 </thead>
						 <tbody>
						 </tbody>
					</table>

                    <div class="col-sm-6 col-xs-12 pull-left">
					<figure>
                        <img style="margin-top:-4px;" src= <?php echo $servidor."vistas/img/iconos/compra-segura.jpg" ?> class="img-thumbnail">
                        </figure>
					</div>
					<div class="col-sm-6 col-xs-12 pull-right">
						<!-- subtabla de totales -->
						<table class="table table-striped tablaTasas">
							
							<tbody>
								
								<tr>
									<td>Subtotal</td>	
									<td><span class="cambioDivisa">MXN</span> $<span class="valorSubtotal" valor="0">0</span></td>	
								</tr>

								<tr>
									<td>Envío</td>	
									<td><span class="cambioDivisa">MXN</span> $<span class="valorTotalEnvio" valor="0">0</span></td>	
								</tr>

								<tr>
									<td>Impuesto</td>	
									<td><span class="cambioDivisa">MXN</span> $<span class="valorTotalImpuesto" valor="0">0</span></td>	
								</tr>

								<tr>
									<td><strong>Total</strong></td>	
									<td>
									    <strong><span class="cambioDivisa">MXN</span> 
										$<span class="valorTotalCompra" valor="0">0</span></strong>
									</td>	
								</tr>

							</tbody>	

						</table>
						<div class="descuentoAplicado" 
						     style="display:none;margin-top:-18px;margin-left: 8px;background-color: red;color: white;border-radius: 5px;text-align: center;">$0.00</div>

						 <!-- <div class="divisa">
						 	<select class="form-control" id="cambiarDivisa" name="divisa">
						 	</select>	
						 	<br>
						 </div> -->
					</div>

					<div class="clearfix"></div>
					<button style="margin-top:5px" class="btn btn-block btn-lg btn-default backColor btnPagar">PAGAR</button>
					<br>
				</div>

			</div>

		</div>

		<!-- <div class="modal-footer">
      	
      	</div> -->

	</div>

</div>
