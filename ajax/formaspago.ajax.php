<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

class AjaxFormaspago{

/*=============================================
	OBTENER FORMAS DE PAGO
	=============================================*/
	public $id;	
	public function ajaxGetFormaspago(){
		$datos = $this->id;
		$item = "id_usuario";
		//$valor = $_POST["id"];
		$respuesta = ControladorUsuarios::ctrMostrarformasPago($item, $datos);
		echo json_encode($respuesta);
	}
}
/*=============================================
OBTENER LISTA DE FORMAS DE PAGO
=============================================*/

if(isset($_POST["id"])){

	$obtenerDomicilios = new AjaxFormaspago();
	$obtenerDomicilios -> id = $_POST["id"];
	$obtenerDomicilios ->ajaxGetFormaspago();
}
