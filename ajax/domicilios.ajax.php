<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

class AjaxDomicilios{

/*=============================================
	OBTENER DOMICILIOS
	=============================================*/
	public $id;	
	public function ajaxGetDomicilios(){
		$datos = $this->id;
		$item = "id_usuario";
		//$valor = $_POST["id"];
		$respuesta = ControladorUsuarios::ctrMostrarDirecciones($item, $datos);
		echo json_encode($respuesta);
	}
}
/*=============================================
OBTENER LISTA DE DOMICILIOS
=============================================*/

if(isset($_POST["id"])){

	$obtenerDomicilios = new AjaxDomicilios();
	$obtenerDomicilios -> id = $_POST["id"];
	$obtenerDomicilios ->ajaxGetDomicilios();
}
